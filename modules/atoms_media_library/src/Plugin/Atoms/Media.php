<?php

namespace Drupal\atoms_media_library\Plugin\Atoms;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsPluginBase;
use Drupal\atoms\ViewableAtom;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

/**
 * Media Library plugin for atoms.
 *
 * @Atoms(
 *  id = "media",
 *  title = @Translation("Media"),
 *  description = @Translation("Media"),
 *  types = {
 *    "media"
 *  }
 * )
 */
class Media extends AtomsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getTypeNames() {
    return [
      'media' => 'Media',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasTokenSupport() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuilder(Atom $atom) {
    $entity_type = 'media';
    $entities = $this->value($atom->view());

    $form = [
      '#type' => 'media_library',
      '#title' => $atom->getTitle(),
      '#description' => $atom->getDescription(),
      '#target_type' => $entity_type,
      '#allowed_bundles' => ['image'],
    ];

    if ($atom->getOptions()['multiple'] ?? FALSE) {
      $form['#tags'] = TRUE;
      $form['#default_value'] = $entities;
    }
    else {
      $form['#default_value'] = empty($entities) ? NULL : reset($entities)->id();
    }

    if (!empty($atom->getOptions()['target_bundles'])) {
      $form['#allowed_bundles'] = $atom->getOptions()['target_bundles'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function value(ViewableAtom $view, $key = '') {
    $entity_type = 'media';
    $entity_ids = $view->getData() ?? [];
    if (!is_array($entity_ids)) {
      $entity_ids = [$entity_ids];
    }
    try {
      $entities = \Drupal::entityTypeManager()
        ->getStorage($entity_type)
        ->loadMultiple($entity_ids);
    }
    catch (InvalidPluginDefinitionException) {
      \Drupal::logger('atoms')
        ->error('Atom (' . $view->getAtom()
          ->getMachineName() . ') of type ' . $view->getType() . ' caused an InvalidPluginDefinitionException');
      $entities = [];
    }
    catch (PluginNotFoundException) {
      \Drupal::logger('atoms')
        ->error('Atom (' . $view->getAtom()
          ->getMachineName() . ') of type ' . $view->getType() . ' caused an PluginNotFoundException');
      $entities = [];
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(Atom $atom, FormStateInterface $form_state) {
    $value = $form_state->getValue($this->getFormStateKey($atom));
    $entity_ids = [];
    if (is_array($value)) {
      foreach ($value as $array) {
        $entity_ids[] = $array['target_id'];
      }
    }
    else {
      $entity_ids[] = intval($value);
    }
    $atom->setData($entity_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function renderBuild(ViewableAtom $view, array &$build) {
    $entity_type = 'media';
    $entities = $this->value($view);
    foreach ($entities as $entity) {
      $this->bubbleableMetadata->addCacheableDependency($entity);
    }
    $view_mode = $view->getAtom()
      ->getOptions()['view_mode'] ?? ($this->getConfig()
        ->get($this->getPluginId() . '_view_mode') ?? 'full');

    $build += \Drupal::entityTypeManager()
      ->getViewBuilder($entity_type)
      ->viewMultiple($entities, $view_mode, $view->getLangCode());
  }

  /**
   * {@inheritdoc}
   */
  public function summary(Atom $atom) {
    $entity_type = 'media';
    $entities = $this->value($atom->view());
    $view_mode = $atom->getOptions()['summary_view_mode'] ?? ($this->getConfig()
      ->get($this->getPluginId() . '_summary_view_mode') ?? 'media_library');

    return \Drupal::entityTypeManager()
      ->getViewBuilder($entity_type)
      ->viewMultiple($entities, $view_mode, $atom->getLangCode());
  }

  /**
   * {@inheritdoc}
   */
  public function values(ViewableAtom $view, $key = '') {
    $entity_ids = $view->getData() ?? [];
    if (!is_array($entity_ids)) {
      $entity_ids = [$entity_ids];
    }
    return ['target_ids' => $entity_ids];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(Config $config, FormStateInterface $form_state) {
    // Call the entity service.
    /** @var \Drupal\Core\Entity\EntityDisplayRepository $entityDisplay */
    $entityDisplay = \Drupal::service('entity_display.repository');

    // Call the necessary method in order to return 'media' view modes.
    $viewModes = $entityDisplay->getViewModes('media');

    $options = [];
    /** @var \Drupal\Core\Datetime\Entity\DateFormat $format */
    foreach ($viewModes as $id => $viewMode) {
      $options[$id] = $viewMode['label'];
    }

    $form[$this->getPluginId() . '_view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Default view mode'),
      '#options' => $options,
      '#default_value' => $config->get($this->getPluginId() . '_view_mode') ?: 'full',
    ];

    $form[$this->getPluginId() . '_summary_view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Summary view mode'),
      '#options' => $options,
      '#default_value' => $config->get($this->getPluginId() . '_summary_view_mode') ?: 'media_library',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsValidate(Config $config, FormStateInterface $form_state) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSubmit(Config $config, FormStateInterface $form_state) {
    $config->set($this->getPluginId() . '_view_mode', $form_state->getValue($this->getPluginId() . '_view_mode'));
    $config->set($this->getPluginId() . '_summary_view_mode', $form_state->getValue($this->getPluginId() . '_summary_view_mode'));
  }

}
