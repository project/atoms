<?php

namespace Drupal\atoms\Transform;

use Drupal\Core\Language\LanguageInterface;
use Drupal\transform_api\Transform\PluginTransformBase;

/**
 * Transformation for atoms.
 */
class AtomTransform extends PluginTransformBase {

  /**
   * Construct an AtomTransform.
   *
   * @param string|array $machineNames
   *   Either a single machine name or an array of machine names for atom(s).
   */
  public function __construct($machineNames, $langcode = NULL) {
    $this->values = [
      'machineNames' => $machineNames,
      'langcode' => $langcode ?? \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId()
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTransformType() {
    return 'atom';
  }

  /**
   * {@inheritdoc}
   */
  public function shouldBeCached() {
    return !$this->isMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function isMultiple() {
    return is_array($this->getValue('machineNames'));
  }

}
