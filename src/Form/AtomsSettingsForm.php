<?php

namespace Drupal\atoms\Form;

use Drupal\atoms\AtomsManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for atoms.
 */
class AtomsSettingsForm extends ConfigFormBase {

  /**
   * The atoms plugin manager.
   *
   * @var \Drupal\atoms\AtomsManager
   */
  protected $manager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\atoms\AtomsManager $manager
   *   The atoms plugin manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AtomsManager $manager
  ) {
    parent::__construct($config_factory);
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.atoms.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'AtomsSettingsForm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('atoms.settings');

    $form['tokens'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable token support'),
      '#default_value' => $config->get('tokens') ?: FALSE,
    ];

    foreach ($this->manager->getDefinitions() as $plugin_id => $definition) {
      try {
        /** @var \Drupal\atoms\AtomsPluginInterface $plugin */
        $plugin = $this->manager->createInstance($plugin_id);
        $plugin_form = $plugin->settingsForm($config, $form_state);
      }
      catch (PluginException) {
        $plugin_form = [];
      }
      if (!empty($plugin_form)) {
        $plugin_form['#type'] = 'fieldset';
        $plugin_form['#title'] = $definition['title'];
        $form[$plugin_id] = $plugin_form;
      }
    }

    return parent::buildform($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('atoms.settings');
    $success = TRUE;
    foreach ($this->manager->getDefinitions() as $plugin_id => $definition) {
      try {
        /** @var \Drupal\atoms\AtomsPluginInterface $plugin */
        $plugin = $this->manager->createInstance($plugin_id);
        $success = $plugin->settingsValidate($config, $form_state) & $success;
      }
      catch (PluginException) {
      }
    }
    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('atoms.settings');

    $config->set('tokens', $form_state->getValue('tokens'));

    foreach ($this->manager->getDefinitions() as $plugin_id => $definition) {
      try {
        /** @var \Drupal\atoms\AtomsPluginInterface $plugin */
        $plugin = $this->manager->createInstance($plugin_id);
        $plugin->settingsSubmit($config, $form_state);
      }
      catch (PluginException) {
      }
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'atoms.settings',
    ];
  }

}
