<?php

namespace Drupal\atoms\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Simple atoms overview form.
 */
class AtomsOverviewListForm extends AtomsOverviewBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'atoms_overview_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['#attached']['library'][] = 'core/drupal.tableresponsive';

    $form['list'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Title'),
        $this->t('Category'),
        $this->t('Fields'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('There are no texts yet.'),
      '#tableselect' => FALSE,
      '#attributes' => ['class' => ["responsive-enabled"]],
    ];
    foreach ($this->atoms as $category => $groups) {
      foreach ($groups as $group => $subgroups) {
        foreach ($subgroups as $atoms) {
          $fields = '';
          /** @var \Drupal\atoms\Atom $atom */
          foreach ($atoms as $atom) {
            if (!isset($form['list'][$group])) {
              $form['list'][$group] = [];
              $form['list'][$group]['title'] = [
                '#type' => 'link',
                '#title' => $group,
                '#url' => Url::fromRoute('atoms.edit', ['group_id' => $atom->getGroupId()]),
              ];
              $form['list'][$group]['category'] = [
                '#plain_text' => $category,
              ];
              $form['list'][$group]['fields'] = ['#plain_text' => ''];
              $fields = '';
            }

            if (!empty($fields)) {
              $fields .= ', ';
            }
            $fields .= $atom->getTitle();

            if (!isset($form['list'][$group]['operations'])) {
              $links = $this->getOperations($atom);
              $form['list'][$group]['operations'] = [
                '#type' => 'operations',
                '#links' => $links,
              ];
            }
          }
          $form['list'][$group]['fields']['#plain_text'] = Unicode::truncate($fields, 160, FALSE, TRUE);
        }
      }
    }

    $form['pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function formRoute() {
    return 'atoms.overview';
  }

  /**
   * {@inheritdoc}
   */
  protected function searchLimit() {
    return 20;
  }

}
