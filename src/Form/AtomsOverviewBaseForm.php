<?php

namespace Drupal\atoms\Form;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsManager;
use Drupal\atoms\AtomsStorage;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Base class for atoms overview forms.
 */
abstract class AtomsOverviewBaseForm extends FormBase {

  /**
   * The atoms storage service.
   *
   * @var \Drupal\atoms\AtomsStorage
   */
  protected $storage;

  /**
   * The atoms plugin manager.
   *
   * @var \Drupal\atoms\AtomsManager
   */
  protected $manager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The list of atoms to display.
   *
   * @var \Drupal\atoms\Atom[]
   */
  protected array $atoms;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs an atom overview.
   */
  public function __construct(AtomsStorage $storage, AtomsManager $manager, RequestStack $requestStack, LanguageManagerInterface $languageManager) {
    $this->storage = $storage;
    $this->manager = $manager;
    $this->requestStack = $requestStack;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('atoms.storage'),
      $container->get('plugin.atoms.manager'),
      $container->get('request_stack'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $category = $this->requestStack->getCurrentRequest()->query->get('category', '');
    $type = $this->requestStack->getCurrentRequest()->query->get('type', '');
    $this->atoms = $this->storage->searchAtoms([
      'langcode' => $langcode,
      'category' => $category,
      'type' => $type,
    ], $this->searchLimit());

    $form['filters'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline', 'clearfix'],
      ],
    ];

    $categories = ['' => $this->t('- Any -')] + $this->storage->getCategories();
    asort($categories);
    $form['filters']['categories'] = [
      '#type' => 'select',
      '#options' => $categories,
      '#title' => $this->t('Category'),
      '#default_value' => $category,
    ];

    $data = $this->manager->getTypeNames();
    $types = ['' => $this->t('- Any -')];
    foreach ($data as $id => $row) {
      $types[$id] = $row;
    }
    asort($types);
    $form['filters']['types'] = [
      '#type' => 'select',
      '#options' => $types,
      '#title' => $this->t('Field types'),
      '#default_value' => $type,
    ];

    $form['filters']['actions'] = [
      '#type' => 'actions',
    ];
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    return $form;
  }

  /**
   * Limit the number of atoms this overview will display.
   *
   * @return int
   *   The number of atoms this overview will display.
   */
  abstract protected function searchLimit();

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];
    if (!empty($form_state->getValue('categories')) && $form_state->getValue('categories') != 'all') {
      $query['category'] = $form_state->getValue('categories');
    }
    if (!empty($form_state->getValue('types')) && $form_state->getValue('types') != 'all') {
      $query['type'] = $form_state->getValue('types');
    }
    $form_state->setRedirect($this->formRoute(), [], ['query' => $query]);
  }

  /**
   * Get the route for the specific overview.
   *
   * @return string
   *   The route for the specific overview.
   */
  abstract protected function formRoute();

  /**
   * Get operation to perform on an atom.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom the operations related to.
   *
   * @return array
   *   List of operations.
   */
  protected function getOperations(Atom $atom) {
    $links = [];
    $links['edit'] = [
      'title' => $this->t('Edit'),
      'url' => Url::fromRoute('atoms.edit', ['group_id' => $atom->getGroupId()]),
    ];
    $links['translate'] = [
      'title' => $this->t('Translate'),
      'url' => Url::fromRoute('atoms.translate.overview', ['group_id' => $atom->getGroupId()]),
    ];
    return $links;
  }

}
