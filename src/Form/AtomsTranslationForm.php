<?php

namespace Drupal\atoms\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form for translating atoms.
 */
class AtomsTranslationForm extends AtomsEditForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'atoms_translation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $group_id = '';
    $atoms = $this->getGroup($form_state);
    foreach ($atoms as $atom) {
      $group_id = $atom->getGroupId();
    }

    $form_state->setRedirect('atoms.translate.overview', [
      'group_id' => $group_id,
    ]);
  }

  /**
   * Get administrative title of a group of atoms.
   *
   * @param string $group_id
   *   The group id for a group of atoms.
   * @param string|null $langcode
   *   (optional) The language code to view the atom with.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated title of the group.
   */
  public function getTitle($group_id, $langcode = NULL) {
    /** @var \Drupal\atoms\AtomsStorage $service */
    $service = \Drupal::service('atoms.storage');
    $group_name = $service->getGroupName($group_id);
    return $this->t('Translate %title',
      ['%title' => $group_name],
      ['langcode' => $langcode]);
  }

}
