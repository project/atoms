<?php

namespace Drupal\atoms\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Detailed atoms overview form.
 */
class AtomsOverviewDetailForm extends AtomsOverviewBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'atoms_overview_detail';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['#attached']['library'][] = 'core/drupal.tableresponsive';
    $type_names = $this->manager->getTypeNames();

    // Iterate over each of the modules.
    $form['atoms']['#tree'] = TRUE;
    foreach ($this->atoms as $category => $groups) {
      $form['atoms'][$category] = [
        '#markup' => '<h2>' . $category . '</h2>',
      ];
      foreach ($groups as $group => $subgroups) {
        $form['atoms'][$category][$group] = [
          '#type' => 'details',
          '#title' => $group,
          '#open' => TRUE,
        ];
        $form['atoms'][$category][$group]['actions'] = [
          '#type' => 'actions',
        ];
        foreach ($subgroups as $subgroup => $atoms) {
          if (!empty($subgroup)) {
            $form['atoms'][$category][$group][$subgroup] = [
              '#type' => 'details',
              '#title' => $subgroup,
            ];
          }
          $form['atoms'][$category][$group][$subgroup]['list'] = [
            '#type' => 'table',
            '#header' => [
              $this->t('Title'),
              $this->t('Type'),
              $this->t('Summary'),
            ],
            '#empty' => $this->t('There are no texts yet.'),
            '#tableselect' => FALSE,
            '#attributes' => ['class' => ["responsive-enabled"]],
          ];
          /** @var \Drupal\atoms\Atom $atom */
          foreach ($atoms as $atom) {
            $form['atoms'][$category][$group][$subgroup]['list'][$atom->getMachineName()]['title'] = [
              '#type' => 'item',
              '#title' => $atom->getTitle(),
              '#description' => $atom->getDescription(),
              '#description_display' => 'after',
            ];
            $form['atoms'][$category][$group][$subgroup]['list'][$atom->getMachineName()]['type'] = [
              '#plain_text' => $type_names[$atom->getType()],
            ];

            $atomsPlugin = $this->manager->createInstanceFromType($atom->getType());
            if (empty($atomsPlugin)) {
              $listing_data = ['#markup' => $this->t("Could not find plugin for field type")];
            }
            else {
              $listing_data = $atomsPlugin->summary($atom);
            }
            $form['atoms'][$category][$group][$subgroup]['list'][$atom->getMachineName()]['summary'] = $listing_data;

            foreach ($this->getOperations($atom) as $id => $link) {
              $form['atoms'][$category][$group]['actions'][$id] = [
                '#type' => 'link',
                '#title' => $link['title'],
                '#url' => $link['url'],
                '#attributes' => ['class' => 'button'],
              ];
            }
          }
        }
      }
    }

    $form['pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function searchLimit() {
    return 5;
  }

  /**
   * {@inheritdoc}
   */
  protected function formRoute() {
    return 'atoms.overview.detail';
  }

}
