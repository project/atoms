<?php

namespace Drupal\atoms\Form;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsStorage;
use Drupal\atoms\AtomsStorageException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for editing atoms.
 */
class AtomsEditForm extends FormBase {

  /**
   * The atoms storage service.
   *
   * @var \Drupal\atoms\AtomsStorage
   */
  protected AtomsStorage $atomsStorage;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Construct AtomsEditForm form.
   *
   * @param \Drupal\atoms\AtomsStorage $atomsStorage
   *   The atoms storage service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(AtomsStorage $atomsStorage, ModuleHandlerInterface $moduleHandler) {
    $this->atomsStorage = $atomsStorage;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = new static(
      $container->get('atoms.storage'),
      $container->get('module_handler')
    );
    $form->setStringTranslation($container->get('string_translation'));
    $form->setMessenger($container->get('messenger'));
    $form->setConfigFactory($container->get('config.factory'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $group_id = NULL, $langcode = NULL) {
    if (empty($group_id)) {
      return $form;
    }

    $atoms = Atom::loadGroup($group_id, $langcode);
    if (empty($atoms)) {
      return $form;
    }
    else {
      $form['group_id'] = [
        '#type' => 'hidden',
        '#value' => $group_id,
      ];
      $form['langcode'] = [
        '#type' => 'hidden',
        '#value' => $langcode,
      ];
    }
    $form['#tree'] = TRUE;

    $subgroups = ['0' => '0'];
    $token_types = [];
    $token_support = FALSE;
    foreach ($atoms as $id => $atom) {
      if ($this->getFormId() == 'atoms_translation_form' && !$atom->isTranslatable()) {
        unset($atoms[$id]);
        continue;
      }
      if (!empty($atom->getSubgroupName())) {
        $subgroups[$atom->getSubgroupId()] = $atom->getSubgroupName();
      }
      if ($atom->getPlugin()->hasTokenSupport()) {
        $token_support = TRUE;
        if (!empty($atom->getTokenTypes())) {
          $token_types = array_merge($token_types, $atom->getTokenTypes());
        }
      }
    }
    if (ksort($subgroups)) {
      foreach ($subgroups as $id => $subgroup) {
        if (empty($id)) {
          $form[$id] = [
            '#type' => 'container',
            '#tree' => TRUE,
          ];
        }
        else {
          $form[$id] = [
            '#type' => 'fieldset',
            '#title' => $subgroup,
          ];
        }
      }
    }
    foreach ($atoms as $atom) {
      if (empty($atom->getSubgroupName())) {
        $subgroup = '0';
      }
      else {
        $subgroup = $atom->getSubgroupId();
      }

      if (!empty($langcode) && $atom->isTranslatable()) {
        $atom->setLangcode($langcode);
      }
      $form[$subgroup][$atom->getMachineName()] = $atom->getPlugin()
        ->formBuilder($atom);
      $form[$subgroup][$atom->getMachineName()]['#weight'] = $atom->getWeight();
    }

    if ($this->moduleHandler
      ->moduleExists('token') && $this->config('atoms.settings')
      ->get('tokens') && $token_support) {
      $form['token_help'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => $token_types,
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save changes'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'atoms_edit_form';
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $atoms = $this->getGroup($form_state);
    foreach ($atoms as $atom) {
      $atom->getPlugin()->validate($form, $form_state);
    }
  }

  /**
   * Get a group of atoms with the current language.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   *
   * @return \Drupal\atoms\Atom[]
   *   Array of atoms in the current language.
   */
  protected function getGroup(FormStateInterface $formState) {
    $group_id = $formState->getValue('group_id');
    $langcode = empty($formState->getValue('langcode')) ? NULL : $formState->getValue('langcode');
    $atoms = Atom::loadGroup($group_id, $langcode);
    foreach ($atoms as $atom) {
      if (!empty($langcode)) {
        $atom->setLangcode($langcode);
      }
    }
    return $atoms;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (isset($form_state->getUserInput()['op'])) {
      $success = TRUE;
      $category = '';
      $group = '';
      $atoms = $this->getGroup($form_state);
      foreach ($atoms as $atom) {
        $atom->getPlugin()->submit($atom, $form_state);
        $category = $atom->getCategory();
        $group = $atom->getGroupName();
        try {
          if (!$atom->save()) {
            $success = FALSE;
          }
        }
        catch (AtomsStorageException) {
          $success = FALSE;
        }
        Cache::invalidateTags(['atoms:' . $atom->getMachineName()]);
      }
      if ($success) {
        $this->messenger()
          ->addStatus($this->t('@category: %group saved!', [
            '@category' => $category,
            '%group' => $group,
          ]));
      }
      else {
        $this->messenger()
          ->addError($this->t('An error occurred saving @category: %group', [
            '@category' => $category,
            '%group' => $group,
          ]));
      }
      $form_state->setRedirect('atoms.overview');
    }
  }

  /**
   * Get administrative title of a group of atoms.
   *
   * @param string $group_id
   *   The group id for a group of atoms.
   * @param string|null $langcode
   *   (optional) The language code to view the atom with.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated title of the group.
   */
  public function getTitle($group_id, $langcode = NULL) {
    $group_name = $this->atomsStorage->getGroupName($group_id);
    return $this->t('Edit %label', [
      '%label' => $group_name,
    ],
      ['langcode' => $langcode]);
  }

}
