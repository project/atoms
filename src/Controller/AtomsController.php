<?php

namespace Drupal\atoms\Controller;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsStorage;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for handling atom routes.
 */
class AtomsController extends ControllerBase {

  /**
   * The atoms storage service.
   *
   * @var \Drupal\atoms\AtomsStorage
   */
  protected $storage;
  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * Construct a AtomsController object.
   *
   * @param \Drupal\atoms\AtomsStorage $storage
   *   The atoms storage service.
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliteration
   *   The transliteration service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(AtomsStorage $storage, TransliterationInterface $transliteration, LanguageManagerInterface $languageManager) {
    $this->storage = $storage;
    $this->transliteration = $transliteration;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('atoms.storage'),
      $container->get('transliteration'),
      $container->get('language_manager')
    );
  }

  /**
   * Translation overview for a group of atoms.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   Render array.
   */
  public function translatableOverview(Request $request) {
    $group_id = $request->get('group_id');
    $group = Atom::loadGroup($group_id);
    $languages = $this->languageManager->getNativeLanguages();
    $rows = [];

    $header = [
      'title' => $this->t('Language'),
      'operations' => $this->t('Operations'),
    ];

    foreach ($languages as $language) {
      $language_exists = FALSE;
      foreach ($group as $atom) {
        $language_exists = $language_exists || $this->storage->languageExists($atom->getMachineName(), $language->getId());
      }
      $link_title = ($language_exists) ? $this->t('Edit') : $this->t('Translate');

      $rows[] = [
        'title' => $this->t($language->getName()),
        'operations' => Link::createFromRoute($link_title, 'atoms.translate', [
          'group_id' => $group_id,
          'langcode' => $language->getId(),
        ]),
      ];
    }

    return [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No translations available.'),

    ];
  }

}
