<?php

namespace Drupal\atoms;

/**
 * Trait for adding atoms support in other classes.
 */
trait AtomsTrait {

  /**
   * Returns a render array representation of the object.
   *
   * @param string $machine_name
   *   The machine name of the atom to load.
   * @param string|null $langcode
   *   (optional) The language code to load the atom with.
   *
   * @return array
   *   A render array to render the atom.
   */
  public function atom($machine_name, $langcode = NULL) {
    return $this->getAtoms()->get($machine_name, $langcode)->toRenderable();
  }

  /**
   * Get the atoms view builder service.
   *
   * @return AtomsViewBuilder
   *   The atoms view builder service.
   */
  public function getAtoms(): AtomsViewBuilder {
    /** @var AtomsViewBuilder $service */
    static $service = NULL;
    if (is_null($service)) {
      $service = \Drupal::service('atoms');
    }
    return $service;
  }

}
