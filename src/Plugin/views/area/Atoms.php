<?php

namespace Drupal\atoms\Plugin\views\area;

use Drupal\atoms\AtomsTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("atoms")
 */
class Atoms extends AreaPluginBase {

  use AtomsTrait;

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    /** @var \Drupal\atoms\AtomsStorage $service */
    $service = \Drupal::service('atoms.storage');

    $form['atom'] = [
      '#title' => $this->t('Atom'),
      '#type' => 'select',
      '#default_value' => $this->options['atoms'],
      '#options' => $service->getTitles(),
      '#rows' => 6,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      return $this->atom($this->options['atom']);
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['atoms'] = ['default' => ''];
    return $options;
  }

}
