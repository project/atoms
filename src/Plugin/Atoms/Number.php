<?php

namespace Drupal\atoms\Plugin\Atoms;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsPluginBase;
use Drupal\atoms\ViewableAtom;

/**
 * Number plugin for atoms.
 *
 * @Atoms(
 *  id = "number",
 *  title = @Translation("Number"),
 *  description = @Translation("Number fields"),
 *  types = {
 *    "number"
 *  }
 * )
 */
class Number extends AtomsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getTypeNames() {
    return [
      'number' => $this->t('Number'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasTokenSupport() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuilder(Atom $atom) {
    $value = $atom->getData();

    return [
      '#type' => $atom->getType(),
      '#title' => $atom->getTitle(),
      '#description' => $atom->getDescription(),
      '#default_value' => is_array($value) ? $value['value'] : intval($value),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function renderBuild(ViewableAtom $view, array &$build) {
    $number = $this->prepareText($view, $view->getData());
    $build = ['#plain_text' => $number];
  }

  /**
   * {@inheritdoc}
   */
  public function summary(Atom $atom) {
    return ['#plain_text' => $atom->getData()];
  }

  /**
   * {@inheritdoc}
   */
  public function value(ViewableAtom $view, $key = '') {
    return intval($view->getData());
  }

}
