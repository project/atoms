<?php

namespace Drupal\atoms\Plugin\Atoms;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsPluginBase;
use Drupal\atoms\ViewableAtom;
use Drupal\Core\Form\FormStateInterface;

/**
 * Checkbox plugin for atoms.
 *
 * @Atoms(
 *  id = "checkbox",
 *  title = @Translation("Checkbox"),
 *  description = @Translation("Checkbox fields"),
 *  types = {
 *    "checkbox"
 *  }
 * )
 */
class Checkbox extends AtomsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getTypeNames() {
    return [
      'checkbox' => $this->t('Checkbox'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasTokenSupport() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuilder(Atom $atom) {
    $value = (bool) $atom->getData();

    return [
      '#type' => 'checkbox',
      '#title' => $atom->getTitle(),
      '#description' => $atom->getDescription(),
      '#default_value' => $value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submit(Atom $atom, FormStateInterface $form_state) {
    $atom->setData((bool) $form_state->getValue($this->getFormStateKey($atom)));
  }

  /**
   * {@inheritdoc}
   */
  public function renderBuild(ViewableAtom $view, array &$build) {
    $data = $view->getData();

    $boolean = $this->prepareText($view, $data);
    $value = $this->t('Yes');
    if (!is_bool($boolean) || !$boolean) {
      $value = $this->t('No');
    }

    $build = ['#markup' => $value];
  }

  /**
   * {@inheritdoc}
   */
  public function summary(Atom $atom) {
    $boolean = $atom->getData();

    $value = $this->t('Yes');
    if (!is_bool($boolean) || !$boolean) {
      $value = $this->t('No');
    }

    return ['#markup' => $value];
  }

  /**
   * {@inheritdoc}
   */
  public function value(ViewableAtom $view, $key = '') {
    return $view->getData();
  }

}
