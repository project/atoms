<?php

namespace Drupal\atoms\Plugin\Atoms;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsPluginBase;
use Drupal\atoms\ViewableAtom;
use Drupal\Core\Config\Config;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Form\FormStateInterface;

/**
 * DateTime plugin for atoms.
 *
 * @Atoms(
 *  id = "datetime",
 *  title = @Translation("Datetime"),
 *  description = @Translation("Date and time"),
 *  types = {
 *    "date",
 *    "datetime"
 *  }
 * )
 */
class DateTime extends AtomsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getTypeNames() {
    return [
      'date' => $this->t('Date'),
      'datetime' => $this->t('Date and time'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasTokenSupport() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuilder(Atom $atom) {
    return [
      '#type' => 'datetime',
      '#title' => $atom->getTitle(),
      '#description' => $atom->getDescription(),
      '#default_value' => $atom->getData(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function renderBuild(ViewableAtom $view, array &$build) {
    /** @var \Drupal\Core\Datetime\DateFormatter $dateFormatter */
    $dateFormatter = \Drupal::service('date.formatter');
    /** @var \Drupal\Core\Datetime\DrupalDateTime $datetime */
    $datetime = $view->getData();
    if (!empty($datetime)) {
      $format = $view->getOptions()['format'] ?? ($this->getConfig()
        ->get($this->getPluginId() . '_format') ?? 'medium');
      $build = ['#markup' => $dateFormatter->format($datetime->getTimestamp(), $format)];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function summary(Atom $atom) {
    /** @var \Drupal\Core\Datetime\DateFormatter $dateFormatter */
    $dateFormatter = \Drupal::service('date.formatter');
    /** @var \Drupal\Core\Datetime\DrupalDateTime $datetime */
    $datetime = $atom->getData();
    $format = $atom->getOptions()['format'] ?? ($this->getConfig()
      ->get($this->getPluginId() . '_format') ?? 'medium');
    if (empty($datetime)) {
      return [];
    }
    else {
      return ['#markup' => $dateFormatter->format($datetime->getTimestamp(), $format)];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function value(ViewableAtom $view, $key = '') {
    return $view->getData();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(Config $config, FormStateInterface $form_state) {
    $date_types = DateFormat::loadMultiple();
    $date_formatter = \Drupal::service('date.formatter');
    $date_formats = [];
    /** @var \Drupal\Core\Datetime\Entity\DateFormat $format */
    foreach ($date_types as $machine_name => $format) {
      $date_formats[$machine_name] = $this->t('@name format: @date', [
        '@name' => $format->label(),
        '@date' => $date_formatter->format(\Drupal::time()->getRequestTime(), $machine_name),
      ]);
    }

    $form[$this->getPluginId() . '_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Default date format'),
      '#options' => $date_formats,
      '#default_value' => $config->get($this->getPluginId() . '_format') ?: 'medium',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsValidate(Config $config, FormStateInterface $form_state) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSubmit(Config $config, FormStateInterface $form_state) {
    $config->set($this->getPluginId() . '_format', $form_state->getValue($this->getPluginId() . '_format'));
  }

}
