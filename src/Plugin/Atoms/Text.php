<?php

namespace Drupal\atoms\Plugin\Atoms;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsPluginBase;
use Drupal\atoms\ViewableAtom;

/**
 * Text plugin for atoms.
 *
 * @Atoms(
 *  id = "text",
 *  title = @Translation("Text"),
 *  description = @Translation("Text fields"),
 *  types = {
 *    "textfield",
 *    "textarea",
 *    "email"
 *  }
 * )
 */
class Text extends AtomsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getTypeNames() {
    return [
      'textfield' => $this->t('Text (plain)'),
      'textarea' => $this->t('Text (plain, long)'),
      'email' => $this->t('E-mail'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasTokenSupport() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuilder(Atom $atom) {
    $value = $atom->getData();

    $form = [
      '#type' => $atom->getType(),
      '#title' => $atom->getTitle(),
      '#description' => $atom->getDescription(),
      '#default_value' => is_array($value) ? $value['value'] : $value,
      '#maxlength' => 255,
    ];

    if ($this->isTokenSupportEnabled()) {
      $form['#element_validate'] = ['token_element_validate'];
      $form['#token_types'] = $atom->getTokenTypes();
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function renderBuild(ViewableAtom $view, array &$build) {
    $text = $this->prepareText($view, $view->getData());
    $build = ['#plain_text' => $text];
  }

  /**
   * {@inheritdoc}
   */
  public function summary(Atom $atom) {
    return ['#plain_text' => $atom->getData()];
  }

}
