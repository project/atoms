<?php

namespace Drupal\atoms\Plugin\Atoms;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsPluginBase;
use Drupal\atoms\ViewableAtom;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url as CoreUrl;
use Drupal\link\LinkItemInterface;

/**
 * Links plugin for atoms.
 *
 * @Atoms(
 *  id = "link",
 *  title = @Translation("Link"),
 *  description = @Translation("Links and URLs"),
 *  types = {
 *    "link",
 *    "url",
 *    "button"
 *  }
 * )
 */
class Link extends AtomsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getTypeNames() {
    return [
      'url' => $this->t('URL'),
      'link' => $this->t('Link'),
      'button' => $this->t('Button'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasTokenSupport() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuilder(Atom $atom) {
    $value = $atom->getData();
    if (is_array($value)) {
      $value = $value['value'] ?? $value;
      $value = $value['uri'] ?? $value;
      if (is_array($value)) {
        $value = '';
      }
    }
    try {
      $uri_string = static::getUriAsDisplayableString($value);
    }
    catch (InvalidPluginDefinitionException | \TypeError | PluginNotFoundException) {
      $uri_string = '';
    }
    $internal = $atom->getOptions()['internal'] ?? TRUE;
    $external = $atom->getOptions()['external'] ?? TRUE;
    $link_type = (($internal) ? LinkItemInterface::LINK_INTERNAL : 0) & (($external) ? LinkItemInterface::LINK_EXTERNAL : 0);

    $form['item'] = [
      '#type' => 'item',
      '#title' => $atom->getTitle(),
      '#description' => $atom->getDescription(),
    ];

    $form['url'] = [
      '#type' => 'url',
      '#link_type' => $link_type,
      '#default_value' => $uri_string,
      '#maxlength' => 255,
    ];

    if ($internal) {
      $form['url']['#type'] = 'entity_autocomplete';
      // @todo You should be able to select an entity type.
      $form['url']['#target_type'] = 'node';
      // Disable autocompletion when the first character is '/', '#' or '?'.
      $form['url']['#attributes']['data-autocomplete-first-character-blacklist'] = '/#?';

      // The link widget is doing its own processing in
      // static::getUriAsDisplayableString().
      $form['url']['#process_default_value'] = FALSE;
      $form['url']['#element_validate'] = [];
    }

    // If the field is configured to allow only internal links, add a useful
    // element prefix and description.
    if (!$external) {
      $form['url']['#field_prefix'] = rtrim(CoreUrl::fromRoute('<front>', [], ['absolute' => TRUE])
        ->toString(), '/');
      $form['url']['#description'] = $this->t('This must be an internal path such as %add-node. You can also start typing the title of a piece of content to select it. Enter %front to link to the front page.', [
        '%add-node' => '/node/add',
        '%front' => '<front>',
      ]);
    }
    // If the field is configured to allow both internal and external links,
    // show a useful description.
    elseif ($external && $internal) {
      $form['url']['#description'] = $this->t('Start typing the title of a piece of content to select it. You can also enter an internal path such as %add-node or an external URL such as %url. Enter %front to link to the front page.', [
        '%front' => '<front>',
        '%add-node' => '/node/add',
        '%url' => 'https://example.com',
      ]);
    }
    // If the field is configured to allow only external links, show a useful
    // description.
    elseif ($external && !$internal) {
      $form['url']['#description'] = $this->t('This must be an external URL such as %url.', ['%url' => 'https://example.com']);
    }

    if ($this->isTokenSupportEnabled()) {
      $form['url']['#element_validate'] = ['token_element_validate'];
      $form['url']['#token_types'] = $atom->getTokenTypes();
    }

    if ($atom->getType() != 'url') {
      $form['title'] = [
        '#type' => 'textfield',
        '#field_prefix' => $this->t('Link text'),
        '#default_value' => $atom->getData()['title'] ?? '',
        '#maxlength' => 255,
      ];

      if (empty($form['url']['#field_prefix'])) {
        $form['url']['#field_prefix'] = $this->t('URL');
      }
      else {
        $form['url']['#field_prefix'] = $this->t('URL: %prefix', ['%prefix' => $form['url']['#field_prefix']]);
      }

      $form['target'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Open link in new window'),
        '#default_value' => !empty($atom->getData()['target'] ?? ''),
      ];
    }

    return $form;
  }

  /**
   * Gets the URI without the 'internal:' or 'entity:' scheme.
   *
   * The following two forms of URIs are transformed:
   * - 'entity:' URIs: to entity autocomplete ("label (entity id)") strings;
   * - 'internal:' URIs: the scheme is stripped.
   *
   * This method is the inverse of ::getUserEnteredStringAsUri().
   *
   * @param string $uri
   *   The URI to get the displayable string for.
   *
   * @return string
   *   The URI without the 'internal:' or 'entity:' scheme.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see static::getUserEnteredStringAsUri()
   */
  protected static function getUriAsDisplayableString($uri) {
    $scheme = parse_url($uri, PHP_URL_SCHEME);

    // By default, the displayable string is the URI.
    $displayable_string = $uri;

    // A different displayable string may be chosen in case of the 'internal:'
    // or 'entity:' built-in schemes.
    if ($scheme === 'internal') {
      $uri_reference = explode(':', $uri, 2)[1];

      // @todo '<front>' is valid input for BC reasons, may be removed by
      //   https://www.drupal.org/node/2421941
      $path = parse_url($uri, PHP_URL_PATH);
      if ($path === '/') {
        $uri_reference = '<front>' . substr($uri_reference, 1);
      }

      $displayable_string = $uri_reference;
    }
    elseif ($scheme === 'entity') {
      [$entity_type, $entity_id] = explode('/', substr($uri, 7), 2);
      // Show the 'entity:' URI as the entity autocomplete would.
      // @todo Support entity types other than 'node'. Will be fixed in
      //   https://www.drupal.org/node/2423093.
      if ($entity_type == 'node' && $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id)) {
        $displayable_string = EntityAutocomplete::getEntityLabels([$entity]);
      }
    }
    elseif ($scheme === 'route') {
      $displayable_string = ltrim($displayable_string, 'route:');
    }

    return $displayable_string;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(Atom $atom, FormStateInterface $form_state) {
    $values = $form_state->getValue($this->getFormStateKey($atom));
    $value = $values['url'] ?? '';
    $uri_string = static::getUserEnteredStringAsUri($value);
    if ($atom->getType() == 'url') {
      $atom->setData($uri_string);
    }
    else {
      $atom->setData([
        'title' => $values['title'] ?? '',
        'uri' => $uri_string,
        'target' => empty($values['target']) ? '' : '_blank',
      ]);
    }
  }

  /**
   * Gets the user-entered string as a URI.
   *
   * The following two forms of input are mapped to URIs:
   * - entity autocomplete ("label (entity id)") strings: to 'entity:' URIs;
   * - strings without a detectable scheme: to 'internal:' URIs.
   *
   * This method is the inverse of ::getUriAsDisplayableString().
   *
   * @param string $string
   *   The user-entered string.
   *
   * @return string
   *   The URI, if a non-empty $uri was passed.
   *
   * @see static::getUriAsDisplayableString()
   */
  protected static function getUserEnteredStringAsUri($string) {
    // By default, assume the entered string is a URI.
    $uri = trim($string);

    // Detect entity autocomplete string, map to 'entity:' URI.
    $entity_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($string);
    if ($entity_id !== NULL) {
      // @todo Support entity types other than 'node'. Will be fixed in
      //   https://www.drupal.org/node/2423093.
      $uri = 'entity:node/' . $entity_id;
    }
    // Support linking to nothing.
    elseif (in_array($string, ['<nolink>', '<none>'], TRUE)) {
      $uri = 'route:' . $string;
    }
    // Detect a schemeless string, map to 'internal:' URI.
    elseif (!empty($string) && parse_url($string, PHP_URL_SCHEME) === NULL) {
      // @todo '<front>' is valid input for BC reasons, may be removed by
      //   https://www.drupal.org/node/2421941
      // - '<front>' -> '/'
      // - '<front>#foo' -> '/#foo'
      if (str_starts_with($string, '<front>')) {
        $string = '/' . substr($string, strlen('<front>'));
      }
      $uri = 'internal:' . $string;
    }

    return $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function summary(Atom $atom) {
    $value = $atom->getData();
    if (is_array($value)) {
      $value = $value['value'] ?? $value;
      $value = $value['uri'] ?? $value;
    }
    try {
      $url = CoreUrl::fromUri(static::getUserEnteredStringAsUri($value));
    }
    catch (\InvalidArgumentException) {
      $url = FALSE;
    }
    if ($url !== FALSE) {
      if ($atom->getType() == 'url') {
        $string_url = $url->toString(TRUE);
        $build = [
          '#markup' => $string_url->getGeneratedUrl(),
        ];
      }
      else {
        $build = [
          '#type' => 'link',
          '#title' => $atom->getData()['title'],
          '#url' => $url,
        ];
        $build['#attributes'] = [];
        if ($atom->getType() == 'button') {
          $build['#attributes']['class'] = ['button'];
        }
        if (!empty($atom->getData()['target'])) {
          $build['#attributes']['target'] = $atom->getData()['target'];
        }
      }
    }
    else {
      $build = [
        '#plain_text' => 'Invalid URL (' . $value . ')',
      ];
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function renderBuild(ViewableAtom $view, array &$build) {
    $value = $view->getData();
    if (is_array($value)) {
      $value = $value['value'] ?? $value;
      $value = $value['uri'] ?? $value;
      if (is_array($value)) {
        $value = '';
      }
    }
    $value = $this->prepareText($view, $value);
    try {
      $url = CoreUrl::fromUri(static::getUserEnteredStringAsUri($value));
    }
    catch (\InvalidArgumentException) {
      \Drupal::logger('atoms')
        ->error('Atom (' . $view->getAtom()
          ->getMachineName() . ') of type ' . $view->getType() . ' has invalid data');
      $url = FALSE;
    }
    if ($url !== FALSE) {
      if ($view->getType() == 'url') {
        $string_url = $url->toString(TRUE);
        $build = [
          '#markup' => $string_url->getGeneratedUrl(),
        ];
        $this->bubbleableMetadata->addCacheableDependency($string_url);
      }
      else {
        $title = $view->getData()['title'] ?? '';
        $title = $this->prepareText($view, $title);
        $build = [
          '#type' => 'link',
          '#title' => $title,
          '#url' => $url,
        ];
        $build['#attributes'] = [];
        if ($view->getType() == 'button') {
          $build['#attributes']['class'] = $this->getConfig()->get($this->getPluginId() . '_button_class') ?: ['button'];
        }
        if (!empty($view->getData()['target'])) {
          $build['#attributes']['target'] = $view->getData()['target'];
        }
      }
    }
    else {
      $build = [
        '#plain_text' => 'Invalid URL (' . $value . ')',
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function values(ViewableAtom $view) {
    if ($view->getType() == 'url') {
      return [
        'value' => $this->value($view),
      ];
    }
    else {
      return [
        'title' => $this->value($view, 'title'),
        'uri' => $this->prepareText($view, $view->getData()['uri']),
        'target' => $this->value($view, 'target'),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function value(ViewableAtom $view, $key = '') {
    if ($view->getType() == 'url') {
      $uri_string = $this->prepareText($view, $view->getData());
      try {
        $url = CoreUrl::fromUri(static::getUserEnteredStringAsUri($uri_string));
      }
      catch (\InvalidArgumentException) {
        \Drupal::logger('atoms')
          ->error('Atom (' . $view->getAtom()
            ->getMachineName() . ') of type ' . $view->getType() . ' has invalid data');
        $url = FALSE;
      }
      return $url;
    }
    else {
      switch ($key) {
        case 'title':
          return $this->prepareText($view, $view->getData()['title']);

        case 'target':
          return $view->getData()['target'];

        default:
          $uri_string = $this->prepareText($view, $view->getData()['uri']);
          try {
            $url = CoreUrl::fromUri(static::getUserEnteredStringAsUri($uri_string));
          }
          catch (\InvalidArgumentException) {
            \Drupal::logger('atoms')
              ->error('Atom (' . $view->getAtom()
                ->getMachineName() . ') of type ' . $view->getType() . ' has invalid data');
            $url = FALSE;
          }
          return $url;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(Config $config, FormStateInterface $form_state) {
    $classes = $config->get($this->getPluginId() . '_button_class') ?: ['button'];
    $value = implode(', ', $classes);

    $form[$this->getPluginId() . '_button_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button CSS class'),
      '#description' => $this->t('The CSS class used for buttons type Atom. Multiple classes can be specified by separating them with ",".'),
      '#default_value' => $value,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsValidate(Config $config, FormStateInterface $form_state) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSubmit(Config $config, FormStateInterface $form_state) {
    $string = $form_state->getValue($this->getPluginId() . '_button_class');
    $classes = explode(',', $string);
    foreach ($classes as $key => $value) {
      $classes[$key] = trim($value);
    }
    $config->set($this->getPluginId() . '_button_class', $classes);
  }

}
