<?php

namespace Drupal\atoms\Plugin\Atoms;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsPluginBase;
use Drupal\atoms\ViewableAtom;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormStateInterface;

/**
 * Entity reference plugin for atoms.
 *
 * @Atoms(
 *  id = "entity",
 *  title = @Translation("Entity"),
 *  description = @Translation("Entity references"),
 *  types = {
 *    "entity"
 *  }
 * )
 */
class Entity extends AtomsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getTypeNames() {
    return [
      'entity' => $this->t('Entity reference'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasTokenSupport() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuilder(Atom $atom) {
    $entity_type = $this->getEntityType($atom);
    $entities = $this->value($atom->view());

    $form = [
      '#type' => 'entity_autocomplete',
      '#title' => $atom->getTitle(),
      '#description' => $atom->getDescription(),
      '#target_type' => $entity_type,
    ];

    if ($atom->getOptions()['multiple'] ?? FALSE) {
      $form['#tags'] = TRUE;
      $form['#default_value'] = $entities;
    }
    else {
      $form['#default_value'] = empty($entities) ? NULL : reset($entities);
    }

    if (!empty($atom->getOptions()['target_bundles'])) {
      $form['#selection_settings'] = [
        'target_bundles' => $atom->getOptions()['target_bundles'],
      ];
    }

    return $form;
  }

  /**
   * Get the entity type that the atom is referencing.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom we need the entity type reference of.
   *
   * @return string
   *   The entity type that is referenced.
   */
  protected function getEntityType(Atom $atom) {
    return $atom->getOptions()['entity_type'] ?? 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function value(ViewableAtom $view, $key = '') {
    $entity_type = $this->getEntityType($view->getAtom());
    $entity_ids = $view->getData() ?? [];
    if (!is_array($entity_ids)) {
      $entity_ids = [$entity_ids];
    }
    try {
      $entities = \Drupal::entityTypeManager()
        ->getStorage($entity_type)
        ->loadMultiple($entity_ids);
    }
    catch (InvalidPluginDefinitionException) {
      \Drupal::logger('atoms')
        ->error('Atom (' . $view->getAtom()
          ->getMachineName() . ') of type ' . $view->getType() . ' caused an InvalidPluginDefinitionException');
      $entities = [];
    }
    catch (PluginNotFoundException) {
      \Drupal::logger('atoms')
        ->error('Atom (' . $view->getAtom()
          ->getMachineName() . ') of type ' . $view->getType() . ' caused an PluginNotFoundException');
      $entities = [];
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(Atom $atom, FormStateInterface $form_state) {
    $value = $form_state->getValue($this->getFormStateKey($atom));
    $entity_ids = [];
    if (is_array($value)) {
      foreach ($value as $array) {
        $entity_ids[] = $array['target_id'];
      }
    }
    else {
      $entity_ids[] = $value;
    }
    $atom->setData($entity_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function renderBuild(ViewableAtom $view, array &$build) {
    $entity_type = $this->getEntityType($view->getAtom());
    $entities = $this->value($view);
    foreach ($entities as $entity) {
      $this->bubbleableMetadata->addCacheableDependency($entity);
    }
    $view_mode = $view->getAtom()->getOptions()['view_mode'] ?? 'full';

    $build += \Drupal::entityTypeManager()
      ->getViewBuilder($entity_type)
      ->viewMultiple($entities, $view_mode, $view->getLangCode());
  }

  /**
   * {@inheritdoc}
   */
  public function summary(Atom $atom) {
    $entity_type = $this->getEntityType($atom);
    $entities = $this->value($atom->view());
    $view_mode = $atom->getOptions()['summary_view_mode'] ?? 'teaser';

    return \Drupal::entityTypeManager()
      ->getViewBuilder($entity_type)
      ->viewMultiple($entities, $view_mode, $atom->getLangCode());
  }

  /**
   * {@inheritdoc}
   */
  public function values(ViewableAtom $view, $key = '') {
    $entity_ids = $view->getData() ?? [];
    if (!is_array($entity_ids)) {
      $entity_ids = [$entity_ids];
    }
    return ['target_ids' => $entity_ids];
  }

}
