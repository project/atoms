<?php

namespace Drupal\atoms\Plugin\Atoms;

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsPluginBase;
use Drupal\atoms\ViewableAtom;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

/**
 * Text format plugin for atoms.
 *
 * @Atoms(
 *  id = "text_format",
 *  title = @Translation("Text format"),
 *  description = @Translation("Rich Text fields"),
 *  types = {
 *    "text_format"
 *  }
 * )
 */
class TextFormat extends AtomsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getTypeNames() {
    return [
      'text_format' => $this->t('Text (formatted, long)'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasTokenSupport() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuilder(Atom $atom) {
    $value = $atom->getData();

    $form = [
      '#type' => $atom->getType(),
      '#title' => $atom->getTitle(),
      '#description' => $atom->getDescription(),
      '#default_value' => is_array($value) ? $value['value'] : $value,
    ];

    $config = $this->getConfig();
    if (!empty($atom->getOptions()['format'])) {
      $form['#format'] = $atom->getOptions()['format'];
    }
    elseif (!empty($config->get($this->getPluginId() . '_default'))) {
      $form['#format'] = $config->get($this->getPluginId() . '_default');
    }
    if (!empty($atom->getOptions()['allowed_formats'])) {
      $form['#allowed_formats'] = $atom->getOptions()['allowed_formats'];
    }
    elseif (!empty($config->get($this->getPluginId() . '_allowed'))) {
      $form['#allowed_formats'] = $config->get($this->getPluginId() . '_allowed');
    }

    if (isset($value['format'])) {
      $form['#format'] = $value['format'];
    }

    if ($this->isTokenSupportEnabled()) {
      $form['#element_validate'] = ['token_element_validate'];
      $form['#token_types'] = $atom->getTokenTypes();
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function renderBuild(ViewableAtom $view, array &$build) {
    $text = $view->getData();
    if (is_string($text)) {
      $text = [
        'value' => $text,
        'format' => 'full_html',
      ];
    }
    if (is_array($text)) {
      $check = check_markup($text['value'], $text['format']);
      if (is_object($check)) {
        $markup = $check->__toString();
      }
      else {
        $markup = $check;
      }
      $build['#markup'] = $this->prepareText($view, $markup);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function summary(Atom $atom) {
    $text = $atom->getData();
    if (is_array($text)) {
      return ['#markup' => Unicode::truncate($text['value'], 200, TRUE, TRUE)];
    }
    return ['#markup' => Unicode::truncate($text, 200, TRUE, TRUE)];
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(Config $config, FormStateInterface $form_state) {
    $options = [];
    foreach (filter_formats() as $id => $format) {
      $options[$id] = $format->label();
    }

    $form[$this->getPluginId() . '_allowed'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed text formats'),
      '#description' => $this->t('Restrict which text formats are allowed, given the user has the required permissions. If no text formats are selected, then all the ones the user has access to will be available.'),
      '#options' => $options,
      '#default_value' => $config->get($this->getPluginId() . '_allowed') ?: [],
    ];

    $options = ['' => $this->t('- None -')];
    foreach (filter_formats() as $id => $format) {
      $options[$id] = $format->label();
    }

    $form[$this->getPluginId() . '_default'] = [
      '#type' => 'select',
      '#title' => $this->t('Default text format'),
      '#options' => $options,
      '#default_value' => $config->get($this->getPluginId() . '_default') ?: "",
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsValidate(Config $config, FormStateInterface $form_state) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSubmit(Config $config, FormStateInterface $form_state) {
    $config->set($this->getPluginId() . '_default', $form_state->getValue($this->getPluginId() . '_default'));
    $formats = [];
    foreach ($form_state->getValue($this->getPluginId() . '_allowed') as $format => $value) {
      if (!empty($value)) {
        $formats[] = $format;
      }
    }
    $config->set($this->getPluginId() . '_allowed', $formats);
  }

}
