<?php

namespace Drupal\atoms\Plugin\Transform\Type;

use Drupal\atoms\Atom as AtomItem;
use Drupal\atoms\Transform\AtomTransform;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\transform_api\EventSubscriber\TransformationCache;
use Drupal\transform_api\Transform\TransformInterface;
use Drupal\transform_api\TransformationTypeBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Transformation type for atoms.
 *
 * @TransformationType(
 *  id = "atom",
 *  title = "Atom transform"
 * )
 */
class Atom extends TransformationTypeBase {

  /**
   * The transformation cache.
   *
   * @var \Drupal\transform_api\EventSubscriber\TransformationCache
   */
  protected TransformationCache $transformationCache;

  /**
   * Construct an Atom plugin object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransformationCache $transformationCache) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transformationCache = $transformationCache;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('transform_api.transformation_cache')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function transform(TransformInterface $transform) {
    if (is_array($transform->getValue('machineNames'))) {
      return $this->prepareMultipleTransforms($transform->getValue('machineNames'), $transform->getValue('langcode'));
    }
    else {
      return $this->loadAndTransformAtom($transform->getValue('machineNames'), $transform->getValue('langcode'));
    }
  }

  /**
   * Prepare multiple atom transformations at once.
   *
   * @param array $machineNames
   *   The array of machine names to transform.
   * @param string $langcode
   *   Language code for translatable atoms.
   *
   * @return array
   *   Array of transform arrays indexed by machine name.
   */
  public function prepareMultipleTransforms(array $machineNames, $langcode) {
    $transformation = [];
    foreach ($machineNames as $machineName) {
      $transform = new AtomTransform($machineName, $langcode);
      $atom_transformation = $this->transformationCache->get($transform);
      if ($atom_transformation === FALSE) {
        $transformation[$machineName] = $transform;
      }
      else {
        $transformation[$machineName] = $atom_transformation;
      }
    }

    return $transformation;
  }

  /**
   * Load or fetch an atom from cache.
   *
   * @param string $machineName
   *   The machine name for the atom to load or fetch from cache.
   * @param $langcode
   *   Language code for translatable atoms.
   *
   * @return array
   *   Transform array for the atom.
   */
  public function loadAndTransformAtom($machineName, $langcode) {
    $atom = AtomItem::load($machineName, $langcode);
    if (!empty($atom)) {
      $cacheMetadata = CacheableMetadata::createFromObject($atom);
      $transform = [
        'type' => 'atom',
        'machine_name' => $atom->getMachineName(),
        'atom_type' => $atom->getType(),
      ];
      if ($atom->isTranslatable()) {
        $atom = $atom->getTranslation($langcode);
        $transform['langcode'] = $langcode;
      }
      $data = $atom->view()->getValues();
      if (is_array($data)) {
        $transform += $data;
      }
      else {
        $transform['value'] = $data;
      }
      $cacheMetadata->applyTo($transform);
      return $transform;
    }
    else {
      return [];
    }
  }

}
