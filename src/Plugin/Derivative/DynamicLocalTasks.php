<?php

namespace Drupal\atoms\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines dynamic local tasks.
 */
class DynamicLocalTasks extends DeriverBase {

  use StringTranslationTrait;

  /**
   * The atoms storage service.
   *
   * @var \Drupal\atoms\AtomsStorage
   */
  private $storage;

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  private $transliteration;

  /**
   * Array of all atom categories.
   *
   * @var array
   */
  private $categories;

  /**
   * Construct a DynamicLocalTasks object.
   */
  public function __construct() {
    $this->storage = \Drupal::service('atoms.storage');
    $this->transliteration = \Drupal::service('transliteration');
    $this->categories = $this->storage->getCategories();
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['atoms'] = $base_plugin_definition;
    $this->derivatives['atoms']['title'] = $this->t('Atoms');
    $this->derivatives['atoms']['route_name'] = 'atoms.overview';
    $this->derivatives['atoms']['base_route'] = "system.admin_content";

    $this->derivatives['atoms.overview.list'] = $base_plugin_definition;
    $this->derivatives['atoms.overview.list']['title'] = $this->t('Table');
    $this->derivatives['atoms.overview.list']['parent_id'] = 'atoms.local_tasks:atoms';
    $this->derivatives['atoms.overview.list']['route_name'] = 'atoms.overview';

    $this->derivatives['atoms.overview.detail'] = $base_plugin_definition;
    $this->derivatives['atoms.overview.detail']['title'] = $this->t('Detail');
    $this->derivatives['atoms.overview.detail']['parent_id'] = 'atoms.local_tasks:atoms';
    $this->derivatives['atoms.overview.detail']['route_name'] = 'atoms.overview.detail';
    $this->derivatives['atoms.overview.detail']['base_route'] = "system.admin_content";

    return $this->derivatives;
  }

}
