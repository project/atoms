<?php

namespace Drupal\atoms\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines dynamic local tasks.
 */
class LanguageLocalTasks extends DeriverBase {

  use StringTranslationTrait;

  /**
   * Installed languages.
   *
   * @var \Drupal\Core\Language\LanguageInterface[]
   */
  private $languages;

  /**
   * The atoms storage service.
   *
   * @var \Drupal\atoms\AtomsStorage
   */
  private $storage;

  /**
   * Construct a LanguageLocalTasks object.
   */
  public function __construct() {
    $this->languages = \Drupal::languageManager()->getNativeLanguages();
    $this->storage = \Drupal::service('atoms.storage');
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $groups = $this->storage->getGroups();

    foreach ($groups as $group_id => $group_name) {
      foreach ($this->languages as $language) {
        $route_id = $group_id . '.' . $language->getId();
        $this->derivatives[$route_id] = $this->buildRoute($group_id, $language, $base_plugin_definition);
      }
    }

    return $this->derivatives;
  }

  /**
   * Build a route to translate an atom to a certain language.
   *
   * @param string $group_id
   *   The atom group id.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language to translate to.
   * @param array $base_plugin_definition
   *   The definition array of the base plugin.
   *
   * @return array
   *   Route definition array.
   */
  private function buildRoute($group_id, LanguageInterface $language, $base_plugin_definition) {
    $route = [
      'title' => $language->getName(),
      'route_name' => 'atoms.translate',
      'parent_id' => 'atoms.edit',
      'route_parameters' => [
        'group_id' => $group_id,
        'langcode' => $language->getId(),
      ],
    ];

    return array_merge($base_plugin_definition, $route);
  }

}
