<?php

namespace Drupal\atoms;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Service for building viewable atoms.
 */
class AtomsViewBuilder implements TrustedCallbackInterface {

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private LanguageManagerInterface $languageManager;

  /**
   * Construct a AtomsViewBuilder object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager service.
   */
  public function __construct(LanguageManagerInterface $languageManager) {
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['buildAtom'];
  }

  /**
   * Get render array for a lazy builder to render atom.
   *
   * @param string $machine_name
   *   The machine name of the atom to view.
   * @param string|null $langcode
   *   (optional) The language code to view the atom with.
   *
   * @return array
   *   Render array for atom through a lazy builder.
   */
  public function getLazyBuilder($machine_name, $langcode = NULL) {
    $langcode = (empty($langcode)) ? $this->languageManager
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId() : $langcode;
    return [
      '#lazy_builder' => [
        'atoms:buildAtom',
        [
          'machine_name' => $machine_name,
          'langcode' => $langcode,
        ],
      ],
      '#create_placeholder' => TRUE,
    ];
  }

  /**
   * Builds render array for an atom.
   *
   * @return array
   *   Render array.
   */
  public function buildAtom($machine_name, $langcode) {
    return $this->get($machine_name, $langcode)->toRenderable();
  }

  /**
   * Get a ViewableAtom object.
   *
   * @param string $machine_name
   *   The machine name of the atom to view.
   * @param string|null $langcode
   *   (optional) The language code to view the atom with.
   *
   * @return ViewableAtom
   *   ViewableAtom object.
   */
  public function get($machine_name, $langcode = NULL) {
    $views = &drupal_static(__FUNCTION__, []);

    $langcode = (empty($langcode)) ? $this->languageManager
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId() : $langcode;

    $key = $machine_name . ':' . $langcode;
    if (empty($views[$key])) {
      $views[$key] = new ViewableAtom($machine_name, $langcode);
    }
    return $views[$key];
  }

}
