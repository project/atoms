<?php

namespace Drupal\atoms;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Storage class for atoms.
 *
 * @package Drupal\atoms
 */
class AtomsStorage {

  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  private $transliteration;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private LanguageManagerInterface $languageManager;

  /**
   * AtomsStorage constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger factory for getting the logger.
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliteration
   *   The transliteration service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(Connection $connection, LoggerChannelFactoryInterface $loggerChannelFactory, TransliterationInterface $transliteration, LanguageManagerInterface $languageManager) {
    $this->connection = $connection;
    $this->logger = $loggerChannelFactory->get('atoms');
    $this->transliteration = $transliteration;
    $this->languageManager = $languageManager;
  }

  /**
   * Construct a new atom from an array of values.
   *
   * @param array $values
   *   The values to construct the atom from.
   *
   * @return \Drupal\atoms\Atom
   *   The newly constructed atom.
   */
  public function createAtom(array $values) {
    return new Atom($values);
  }

  /**
   * Take an atom and save it to the database.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom that was saved.
   */
  public function save(Atom $atom) {
    if ($atom->isNew()) {
      $machineName = $this->doSaveAtomBase($atom);
      if (!empty($machineName)) {
        $atom->setMachineName($machineName);
      }
    }
    $this->doSaveAtomData($atom);
  }

  /**
   * Save atom base data to the database.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom to save the base data of.
   *
   * @return string|null
   *   The inserted id or NULL.
   */
  private function doSaveAtomBase(Atom $atom) {
    $values = [
      'machine_name' => $atom->getMachineName(),
      'type' => $atom->getType(),
      'title' => (!empty($atom->getTitle(TRUE))) ? $atom->getTitle(TRUE) : $atom->getMachineName(),
      'description' => $atom->getDescription(TRUE),
      'category' => $atom->getCategory(TRUE),
      'group_id' => $atom->getGroupId(),
      'group_name' => $atom->getGroupName(TRUE),
      'subgroup_name' => $atom->getSubgroupName(TRUE),
      'weight' => $atom->getWeight(),
      'options' => serialize($atom->getOptions()),
    ];

    return $this->dbSave('atoms', $values, $atom->isNew());
  }

  /**
   * Get the group name from a group id.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The group name.
   */
  public function getGroupName($group_id) {
    $groupName = $this->connection->select('atoms')
      ->fields('atoms', ['group_name'])
      ->condition('group_id', $group_id)
      ->execute()
      ->fetchField();
    return $this->t($groupName);
  }

  /**
   * Save the data value of an atom.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom to save the data of.
   */
  private function doSaveAtomData(Atom $atom) {
    $values = [
      'machine_name' => $atom->getMachineName(),
      'data' => ($atom->isNew()) ? serialize($atom->getDefault()) : serialize($atom->getData()),
      'langcode' => $atom->getLangcode(),
    ];

    // Remove data if empty.
    if (empty($values['data'])) {
      unset($values['data']);
    }

    // If multilingual default values is defined loop through them and save the
    // values in its own row.
    if ($atom->isNew() && is_array($atom->getDefault())) {
      $languages = $this->languageManager->getLanguages();
      foreach ($atom->getDefault() as $langcode => $data) {
        if (!$atom->isTranslatable()) {
          $langcode = LanguageInterface::LANGCODE_NOT_APPLICABLE;
        }
        if (array_key_exists($langcode, $languages) || $langcode == LanguageInterface::LANGCODE_NOT_APPLICABLE || $langcode == LanguageInterface::LANGCODE_NOT_SPECIFIED) {
          $values['langcode'] = $langcode;
          $values['data'] = serialize($data);
          $this->dbSave('atoms_data', $values, $atom->isNew());
        }
      }
    }
    else {
      $this->dbSave('atoms_data', $values, !$this->languageExists($atom->getMachineName(), $atom->getLangcode()));
    }
  }

  /**
   * Check if an atom has data for a specific language.
   *
   * @param string $machine_name
   *   The machine name of the atom to check.
   * @param string $langcode
   *   The language code to check.
   *
   * @return bool
   *   TRUE if the language exists and FALSE otherwise.
   */
  public function languageExists($machine_name, $langcode) {
    $query = $this->connection->select('atoms_data', 'd');
    $query->condition('d.machine_name', $machine_name)
      ->condition('d.langcode', $langcode);

    $query->addExpression('1');
    $query->range(0, 1);

    try {
      return (bool) $query->execute()->fetchField();
    }
    catch (\Exception) {
      return FALSE;
    }
  }

  /**
   * Rebuild an atom with values from a cache rebuild.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom to rebuild.
   */
  public function rebuild(Atom $atom) {
    $this->doSaveAtomBase($atom);
    if ($atom->isNew()) {
      $this->doSaveAtomData($atom);
    }
  }

  /**
   * Load the data for an atom from the database.
   *
   * @param string $machine_name
   *   The machine name of the atom to load.
   * @param string|null $langcode
   *   (optional) The language code to load the atom with.
   *
   * @return array|null
   *   The values of the atom or NULL if not found.
   */
  public function load($machine_name, $langcode = NULL) {
    $items = $this->loadMultiple([$machine_name], $langcode);
    if (is_array($items)) {
      return reset($items);
    }

    return NULL;
  }

  /**
   * Load data for multiple atoms from the database.
   *
   * @param array $machine_names
   *   The machine names for the atoms to load.
   * @param string|null $langcode
   *   (optional) The language code to load the atoms with.
   *
   * @return array|bool
   *   A machine name indexed array of data or FALSE if unable.
   */
  public function loadMultiple(array $machine_names = [], $langcode = NULL) {
    if (empty($machine_names)) {
      $machine_names = $this->getMachineNames();
    }
    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
    }
    $langcode_list = [
      $langcode,
      LanguageInterface::LANGCODE_NOT_APPLICABLE,
      $this->languageManager->getDefaultLanguage()->getId(),
      LanguageInterface::LANGCODE_NOT_SPECIFIED,
      NULL,
    ];
    if ($langcode == LanguageInterface::LANGCODE_NOT_SPECIFIED || $langcode == LanguageInterface::LANGCODE_NOT_APPLICABLE) {
      array_pop($langcode_list);
    }

    $results = [];
    $missing = $machine_names;
    foreach ($langcode_list as $current_langcode) {
      if (!empty($missing)) {
        $query = $this->connection->select('atoms', 'c');
        $query->innerJoin('atoms_data', 'd', 'c.machine_name = d.machine_name');
        $query->fields('d')
          ->fields('c')
          ->condition('c.machine_name', $missing, 'IN');
        if (!empty($current_langcode)) {
          $query->condition('d.langcode', $current_langcode);
        }

        try {
          $rows = $query->execute();
          while ($values = $rows->fetchAssoc()) {
            $values['isNew'] = FALSE;
            $machine_name = $values['machine_name'];
            $results[$machine_name] = $values;
            if (($key = array_search($machine_name, $missing)) !== FALSE) {
              unset($missing[$key]);
            }
          }
        }
        catch (\Exception) {
          return FALSE;
        }
      }
    }
    if (!empty($missing)) {
      $query = $this->connection->select('atoms', 'c');
      $query->fields('c')
        ->condition('c.machine_name', $missing, 'IN');

      try {
        $rows = $query->execute();
        while ($values = $rows->fetchAssoc()) {
          $values['isNew'] = FALSE;
          $machine_name = $values['machine_name'];
          $results[$machine_name] = $values;
          if (($key = array_search($machine_name, $missing)) !== FALSE) {
            unset($missing[$key]);
          }
        }
      }
      catch (\Exception) {
        return FALSE;
      }
    }
    return $results;
  }

  /**
   * Get an array of machine names belonging to a group.
   *
   * @param string $group_id
   *   The group id to find members of.
   *
   * @return array
   *   Array of machine names.
   */
  public function getMachineNames($group_id = '') {
    if (empty($group_id)) {
      return $this->connection->select('atoms')
        ->fields('atoms', ['machine_name'])
        ->orderBy('machine_name')
        ->execute()
        ->fetchCol();
    }
    else {
      return $this->connection->select('atoms')
        ->fields('atoms', ['machine_name'])
        ->condition('group_id', $group_id)
        ->orderBy('machine_name', 'DESC')
        ->orderBy('subgroup_name', 'DESC')
        ->execute()
        ->fetchCol();
    }
  }

  /**
   * Get an array of all existing group.
   *
   * @return array
   *   Array of group names indexed by group ids.
   */
  public function getGroups() {
    return $this->connection->select('atoms')
      ->fields('atoms', ['group_id', 'group_name'])
      ->groupBy('group_id')
      ->orderBy('group_name')
      ->execute()
      ->fetchAllKeyed();
  }

  /**
   * Get an array of all existing categories.
   *
   * @return array
   *   An indexed array, or an empty array if there is no categories.
   */
  public function getCategories() {
    $query = $this->connection->select('atoms', 'cl');
    $query->fields('cl', ['category'])
      ->groupBy('category')
      ->orderBy('category');
    $items = $query->execute()->fetchCol();
    $categories = [];
    foreach ($items as $category) {
      $categories[$category] = $this->t($category);
    }
    return $categories;
  }

  /**
   * Get a list of available translations for an atom.
   *
   * @param string $machine_name
   *   The machine name of the atom.
   *
   * @return array|bool
   *   List of language codes of the translations available for the atom.
   */
  public function getTranslations($machine_name) {
    $query = $this->connection->select('atoms_data', 'd')
      ->fields('d', ['langcode']);
    $query->condition('d.machine_name', $machine_name);
    try {
      return $query->execute()->fetchCol();
    }
    catch (\Exception) {
      return FALSE;
    }
  }

  /**
   * Delete a translation for an atom.
   *
   * @param string $machine_name
   *   The machine name of the atom.
   * @param string $langcode
   *   The language code of the translation to delete.
   *
   * @return bool
   *   TRUE of translation was deleted or FALSE otherwise.
   */
  public function deleteTranslation(string $machine_name, string $langcode) {
    return $this->connection->delete('atoms_data')
      ->condition('machine_name', $machine_name)
      ->condition('langcode', $langcode)
      ->execute() > 0;
  }

  /**
   * Delete an atom from the database.
   *
   * @param string $machine_name
   *   The machine name of the atom to be deleted.
   *
   * @return bool
   *   TRUE if the atom was deleted and FALSE otherwise.
   */
  public function delete($machine_name) {
    $this->connection->delete('atoms')
      ->condition('machine_name', $machine_name)
      ->execute();
    $this->connection->delete('atoms_data')
      ->condition('machine_name', $machine_name)
      ->execute();
    return TRUE;
  }

  /**
   * Query for atoms based on a set of filters.
   *
   * @param array $params
   *   Parameters to filter the atoms by. You have the following options:
   *   - 'category': The category of atoms you want.
   *   - 'type': The type of atoms you want.
   * @param int $limit
   *   (optional) Limit on the number of resulting atoms.
   *
   * @return Atom[]
   *   List of filtered atoms.
   */
  public function searchAtoms($params = [], $limit = 0) {
    $category = $params['category'] ?? '';
    $type = $params['type'] ?? '';
    $searchResults = [];
    /** @var \Drupal\Core\Database\Query\Select $query */
    $query = $this->connection->select('atoms', 'gc');
    $query->distinct();
    $query->fields('gc', ['category', 'group_name', 'group_id']);
    if (!empty($category)) {
      $query->condition('gc.category', $category);
    }
    if (!empty($type)) {
      $query->condition('gc.type', $type);
    }
    if ($limit != 0) {
      $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit($limit);
    }
    $query
      ->orderBy('gc.category', 'DESC')
      ->orderBy('gc.group_name', 'DESC');
    $result = $query->execute();
    while ($row = $result->fetchAssoc()) {
      $category = $this->t($row['category'])->__toString();
      $group = $this->t($row['group_name'])->__toString();
      if (!isset($searchResults[$category])) {
        $searchResults[$category] = [];
      }
      if (!isset($searchResults[$category][$group])) {
        $searchResults[$category][$group] = [];
        $atoms = Atom::loadGroup($row['group_id']);
        foreach ($atoms as $atom) {
          $subgroup = empty($atom->getSubgroupName(TRUE)) ? '0' : $atom->getSubgroupName(TRUE);
          if (!isset($searchResults[$category][$group][$subgroup])) {
            $searchResults[$category][$group][$subgroup] = [];
          }
          $searchResults[$category][$group][$subgroup][] = $atom;
        }
      }
    }
    return $searchResults;
  }

  /**
   * Get a list of all atom titles.
   *
   * @return array
   *   List of titles indexes by machine names.
   */
  public function getTitles() {
    $texts = [];
    /** @var \Drupal\Core\Database\StatementInterface $result */
    $result = $this->connection->select('atoms')
      ->fields('atoms', ['machine_name', 'title', 'group_name'])
      ->orderBy('group_name', 'title')
      ->execute();
    while ($text = $result->fetchAssoc()) {
      $texts[$text['machine_name']] = $this->t($text['group_name']) . ' - ' . $this->t($text['title']);
    }
    return $texts;
  }

  /**
   * Update database table with array of values.
   *
   * @param string $table
   *   The table to update.
   * @param array $values
   *   The values to update or insert into the table.
   * @param bool $isNew
   *   Whether the data comes from a new atom.
   *
   * @return string|null
   *   The inserted
   */
  private function dbSave($table, array $values, $isNew = TRUE) {
    if (!empty($values)) {
      try {
        if ($values['machine_name'] && !$isNew) {
          // Make sure we don't overwrite values with empty ones.
          $values = array_filter($values, function ($value) {
            return $value !== '';
          });

          if (!empty($values)) {
            $update = $this->connection->update($table)
              ->fields($values)
              ->condition('machine_name', $values['machine_name']);

            if (isset($values['langcode'])) {
              $update->condition('langcode', $values['langcode']);
            }

            $update->execute();
          }
        }
        else {
          return $this->connection->insert($table)->fields($values)->execute();
        }
      }
      catch (\Exception $exception) {
        error_log($exception->getMessage());
        $this->logger->error($exception->getMessage());
      }
    }
    return NULL;
  }

}
