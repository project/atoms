<?php

namespace Drupal\atoms;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for atoms plugin.
 */
class AtomsManager extends DefaultPluginManager {

  /**
   * Constructs a AtomsManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/Atoms',
      $namespaces,
      $module_handler,
      'Drupal\atoms\AtomsPluginInterface',
      'Drupal\atoms\Annotation\Atoms'
    );
    $this->alterInfo('integration_service');
    $this->setCacheBackend($cache_backend, 'atoms_plugins');
  }

  /**
   * Create instance of plugin based on atom type.
   *
   * @param string $type
   *   The type of atom to get plugin for.
   *
   * @return AtomsPluginInterface|null
   *   The plugin or NULL if not found.
   */
  public function createInstanceFromType($type) {
    static $plugins = [];
    $cached = $this->cacheBackend->get($this->cacheKey . ':types');
    if (empty($cached->data)) {
      $this->setCachedAtomTypes();
      $cached = $this->cacheBackend->get($this->cacheKey . ':types');
    }
    if (!empty($cached->data)) {
      $types = $cached->data;

      if (!empty($types[$type])) {
        if (!isset($plugins[$types[$type]])) {
          try {
            $plugins[$types[$type]] = $this->createInstance($types[$type]);
          }
          catch (PluginException) {
            $plugins[$types[$type]] = NULL;
          }
        }
        return $plugins[$types[$type]];
      }
      else {
        $this->setCachedAtomTypes();
      }
    }
    return NULL;
  }

  /**
   * Cache the definitions of atom types.
   */
  public function setCachedAtomTypes() {
    $definitions = $this->getDefinitions();

    $types = [];
    foreach ($definitions as $key => $definition) {
      if (!empty($definition['types'])) {
        foreach ($definition['types'] as $type) {
          $types[$type] = $key;
        }
      }
    }

    $this->cacheBackend->set(
      $this->cacheKey . ':types',
      $types,
      self::getCacheMaxAge()
    );
  }

  /**
   * Returns a list of content types for use in the administration interface.
   *
   * @return array
   *   List of content types
   */
  public function getTypeNames() {
    static $names = [];

    if (empty($names)) {
      $cached = $this->cacheBackend->get($this->cacheKey . ':types:names');
      if (!empty($cached->data)) {
        $names = $cached->data;
      }
      else {
        foreach ($this->getDefinitions() as $plugin_id => $definition) {
          if (!empty($definition['types'])) {
            try {
              /** @var \Drupal\atoms\AtomsPluginInterface $plugin */
              $plugin = $this->createInstance($plugin_id);
              foreach ($plugin->getTypeNames() as $type => $name) {
                $names[$type] = $name;
              }
            }
            catch (PluginException) {
            }
          }
        }
        $this->cacheBackend->set(
          $this->cacheKey . ':types:names',
          $names,
          self::getCacheMaxAge()
              );
      }
    }
    return $names;
  }

}
