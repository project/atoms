<?php

namespace Drupal\atoms\Twig;

use Drupal\atoms\AtomsViewBuilder;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Twig functions for atoms.
 */
class AtomsExtension extends AbstractExtension {

  /**
   * The viewable atoms builder service.
   *
   * @var \Drupal\atoms\AtomsViewBuilder
   */
  protected $viewBuilder;

  /**
   * AtomsExtension constructor.
   *
   * @param \Drupal\atoms\AtomsViewBuilder $viewBuilder
   *   The viewable atoms builder service.
   */
  public function __construct(AtomsViewBuilder $viewBuilder) {
    $this->viewBuilder = $viewBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('atom', [$this, 'atom'], ['is_safe' => ['html']]),
      new TwigFunction('atomString', [$this, 'atomString'], ['is_safe' => ['html']]),
      new TwigFunction('atomLazy', [$this, 'atomLazy'], ['is_safe' => ['html']]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('atom', [$this, 'atom']),
      new TwigFilter('atomString', [$this, 'atomString']),
      new TwigFilter('atomLazy', [$this, 'atomLazy']),
    ];
  }

  /**
   * Provides atoms function to Twig templates.
   *
   * @param string $machine_name
   *   The machine name for the atom instance.
   * @param string|null $langcode
   *   (optional) The language code to render the atom with.
   *
   * @return array
   *   Render array for the atom.
   */
  public function atom($machine_name, $langcode = NULL) {
    return $this->viewBuilder->get($machine_name, $langcode)->toRenderable();
  }

  /**
   * Provides atoms function to Twig templates.
   *
   * @param string $machine_name
   *   The machine name for the atom instance.
   * @param string|null $langcode
   *   (optional) The language code to render the atom with.
   *
   * @return array
   *   Render array for the atom.
   */
  public function atomLazy($machine_name, $langcode = NULL) {
    return $this->viewBuilder->getLazyBuilder($machine_name, $langcode);
  }

  /**
   * Provides atoms function to Twig templates.
   *
   * @param string $machine_name
   *   The machine name for the atom instance.
   * @param string|null $langcode
   *   (optional) The language code to render the atom with.
   *
   * @return string
   *   String markup for the atom.
   */
  public function atomString($machine_name, $langcode = NULL) {
    return $this->viewBuilder->get($machine_name, $langcode)->toString();
  }

}
