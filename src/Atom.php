<?php

namespace Drupal\atoms;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\RenderableInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\TranslatableInterface;

/**
 * Model class for atoms.
 */
class Atom implements CacheableDependencyInterface, TranslatableInterface, RenderableInterface {

  use StringTranslationTrait;

  /**
   * The machine name.
   *
   * @var string
   */
  private $machineName;

  /**
   * The atom data.
   *
   * @var mixed
   */
  private $data;

  /**
   * The language of the data.
   *
   * @var string
   */
  private $langcode;

  /**
   * The type of field to use for editing.
   *
   * @var string
   */
  private $type;

  /**
   * Options for the atom type.
   *
   * @var array
   */
  private $options = [];

  /**
   * Administrative title of the atom.
   *
   * @var string
   */
  private $title;

  /**
   * Administrative description of the atom.
   *
   * @var string
   */
  private $description;

  /**
   * Administrative category of the atom.
   *
   * @var string
   */
  private $category = 'Miscellaneous';

  /**
   * Administrative group name for the atom.
   *
   * @var string
   */
  private $groupName;

  /**
   * ID for the administrative group name.
   *
   * @var string
   */
  private $groupId = '';

  /**
   * Administrative subgroup name for the atom.
   *
   * @var string
   */
  private $subgroupName;

  /**
   * ID for the administrative subgroup name.
   *
   * @var string
   */
  private $subgroupId;

  /**
   * Administrative weight for the atom.
   *
   * @var int
   */
  private $weight = 0;

  /**
   * Default value for the atom.
   *
   * @var mixed
   */
  private $default;

  /**
   * The plugin for the atom type.
   *
   * @var AtomsPluginInterface
   */
  private $plugin;

  /**
   * Whether the atom is a new unsaved atom.
   *
   * @var bool
   */
  private $isNew = TRUE;

  /**
   * Constructs an Atom object.
   *
   * @param array $values
   *   Values for the atom.
   */
  public function __construct(array $values = []) {
    $this->setValues($values);
  }

  /**
   * Clear reference to plugin if serialized.
   */
  public function __sleep() {
    $this->plugin = NULL;
  }

  /**
   * Sets values for the atom.
   *
   * @param array $values
   *   Values for the atom.
   *
   * @return $this
   */
  public function setValues(array $values) {
    foreach ($values as $key => $value) {
      switch ($key) {
        case 'machine_name':
          $this->setMachineName($value);
          break;

        case 'data':
          if ($this->isSerialized($value)) {
            $this->setData(unserialize($value, ['allowed_classes' => FALSE]));
          }
          else {
            $this->setData($value);
          }
          break;

        case 'options':
          if ($this->isSerialized($value)) {
            $this->setOptions(unserialize($value, ['allowed_classes' => FALSE]));
          }
          else {
            $this->setOptions($value);
          }
          break;

        case 'token_types':
          $this->setTokenTypes($value);
          break;

        case 'group_name':
          $this->setGroupName($value);
          break;

        case 'group_id':
          $this->setGroupId($value);
          break;

        case 'subgroup_name':
          $this->setSubgroupName($value);
          break;

        default:
          if (property_exists($this, $key)) {
            $this->$key = $value;
          }
          break;
      }
    }
    return $this;
  }

  /**
   * Checks if string is serialized data.
   *
   * @param string $string
   *   String to check.
   *
   * @return bool
   *   Whether the string is serialized or not.
   */
  public function isSerialized($string) {
    return ($string === 'b:0;' || @unserialize($string, ['allowed_classes' => FALSE]) !== FALSE);
  }

  /**
   * Set what token types are expected to appear in the atom.
   *
   * @param string|array $token_types
   *   Either a comma seperated string or array of token types.
   *
   * @return $this
   */
  public function setTokenTypes($token_types) {
    if (is_string($token_types)) {
      $this->options['token_types'] = explode(',', $token_types);
    }
    elseif (is_array($token_types)) {
      $this->options['token_types'] = $token_types;
    }
    return $this;
  }

  /**
   * Get the raw data of the atom.
   *
   * @return mixed
   *   The raw data of the atom.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Set the raw data for the atom.
   *
   * @param mixed $data
   *   The raw data of the atom.
   *
   * @return $this
   */
  public function setData($data) {
    $this->data = $data;
    return $this;
  }

  /**
   * Get an array of the token types that this atom expects.
   *
   * @return array
   *   Array of token types.
   */
  public function getTokenTypes() {
    if (isset($this->options['token_types']) && is_array($this->options['token_types'])) {
      return $this->options['token_types'];
    }
    else {
      return [];
    }
  }

  /**
   * Get plugin used for this atom.
   *
   * @return AtomsPluginInterface|null
   *   Atom plugin or NULL if plugin could not be found.
   */
  public function getPlugin() {
    if (empty($this->plugin)) {
      /** @var AtomsManager $manager */
      $manager = \Drupal::service('plugin.atoms.manager');
      if ($instance = $manager->createInstanceFromType($this->getType())) {
        $this->plugin = $instance;
      }
    }
    return $this->plugin;
  }

  /**
   * Get the type of the atom.
   *
   * @return string
   *   Atom type identifier.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Set the type of the atom.
   *
   * @param string $type
   *   Atom type identifier.
   *
   * @return $this
   */
  public function setType($type) {
    $this->type = $type;
    return $this;
  }

  /**
   * Get the administrative description of the atom.
   *
   * @param bool $raw
   *   Whether to return the raw or translated value.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The administrative description.
   */
  public function getDescription($raw = FALSE) {
    if ($raw) {
      return $this->description;
    }
    else {
      return $this->t($this->description);
    }
  }

  /**
   * Set the administrative description of the atom.
   *
   * @param string $description
   *   The administrative description.
   *
   * @return $this
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * Get the ID of the administrative group of the atom.
   *
   * @return string
   *   The ID of the administrative group.
   */
  public function getGroupId() {
    if (empty($this->groupId)) {
      $key = $this->category . ':' . $this->groupName;
      /** @var \Drupal\Component\Transliteration\TransliterationInterface $transliteration */
      $transliteration = \Drupal::service('transliteration');
      $group_id = $transliteration->transliterate($key, LanguageInterface::LANGCODE_DEFAULT, '_');
      $group_id = strtolower($group_id);
      $group_id = preg_replace('/[^a-z0-9_]+/', '_', $group_id);
      $group_id = preg_replace('/_+/', '_', $group_id);
      $this->setGroupId($group_id);
    }
    return $this->groupId;
  }

  /**
   * Set the ID of the administrative group of the atom.
   *
   * @param string $groupId
   *   The ID of the administrative group.
   *
   * @return $this
   */
  public function setGroupId($groupId) {
    $this->groupId = $groupId;
    return $this;
  }

  /**
   * Get the administrative category of the atom.
   *
   * @param bool $raw
   *   Whether to return the raw or translated value.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The administrative category of the atom.
   */
  public function getCategory($raw = FALSE) {
    if ($raw) {
      return $this->category;
    }
    else {
      return $this->t($this->category);
    }
  }

  /**
   * Set the administrative category of the atom.
   *
   * @param string $category
   *   The administrative category of the atom.
   *
   * @return $this
   */
  public function setCategory($category) {
    $this->category = $category;
    $this->groupId = '';
    return $this;
  }

  /**
   * Get the administrative group name of the atom.
   *
   * @param bool $raw
   *   Whether to return the raw or translated value.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The administrative group name of the atom.
   */
  public function getGroupName($raw = FALSE) {
    if ($raw) {
      return $this->groupName;
    }
    else {
      return $this->t($this->groupName);
    }
  }

  /**
   * Set the administrative group name of the atom.
   *
   * @param string $groupName
   *   The administrative group name of the atom.
   *
   * @return $this
   */
  public function setGroupName($groupName) {
    $this->groupName = $groupName;
    $this->groupId = '';
    return $this;
  }

  /**
   * Get the ID of the administrative subgroup of the atom.
   *
   * @return string
   *   The ID of the administrative subgroup.
   */
  public function getSubgroupId() {
    if (empty($this->subgroupId)) {
      $key = $this->getSubgroupName();
      /** @var \Drupal\Component\Transliteration\TransliterationInterface $transliteration */
      $transliteration = \Drupal::service('transliteration');
      $group_id = $transliteration->transliterate($key, LanguageInterface::LANGCODE_DEFAULT, '_');
      $group_id = strtolower($group_id);
      $group_id = preg_replace('/[^a-z0-9_]+/', '_', $group_id);
      $group_id = preg_replace('/_+/', '_', $group_id);
      $this->subgroupId = $group_id;
    }
    return $this->subgroupId;
  }

  /**
   * Get the administrative subgroup name of the atom.
   *
   * @param bool $raw
   *   Whether to return the raw or translated value.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The administrative subgroup name of the atom.
   */
  public function getSubgroupName($raw = FALSE) {
    if (empty($this->subgroupName)) {
      return '';
    }
    if ($raw) {
      return $this->subgroupName;
    }
    else {
      return $this->t($this->subgroupName);
    }
  }

  /**
   * Set the administrative subgroup name of the atom.
   *
   * @param string $subgroupName
   *   The administrative subgroup name of the atom.
   *
   * @return $this
   */
  public function setSubgroupName($subgroupName) {
    $this->subgroupName = $subgroupName;
    return $this;
  }

  /**
   * Get the administrative weight of the atom within their group.
   *
   * @return int
   *   The administrative weight of the atom within their group.
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * Set the administrative weight of the atom within their group.
   *
   * @param int $weight
   *   The administrative weight of the atom within their group.
   *
   * @return $this
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * Get the default value of the atom.
   *
   * @return mixed
   *   The default value of the atom.
   */
  public function getDefault() {
    return $this->default;
  }

  /**
   * Set the default value of the atom.
   *
   * @param mixed $default
   *   The default value of the atom.
   *
   * @return $this
   */
  public function setDefault($default) {
    $this->default = $default;
    return $this;
  }

  /**
   * Update the atom with values during a cache rebuild.
   *
   * @return $this
   *
   * @throws \Drupal\atoms\AtomsStorageException
   */
  public function rebuild() {
    if ($storage = $this->getStorage()) {
      $storage->rebuild($this);
    }
    else {
      throw new AtomsStorageException('The data cannot be saved because its not bound to a storage: ' . $this->getMachineName());
    }
    return $this;
  }

  /**
   * Returns the AtomsStorage service.
   *
   * @return AtomsStorage
   *   The AtomsStorage service.
   */
  protected function getStorage() {
    /** @var AtomsStorage $storage */
    $storage = \Drupal::service('atoms.storage');
    return $storage;
  }

  /**
   * Get the administrative title of the atom.
   *
   * @param bool $raw
   *   Whether to return the raw or translated value.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The administrative title of the atom.
   */
  public function getTitle($raw = FALSE) {
    if ($raw) {
      return $this->title;
    }
    else {
      return $this->t($this->title);
    }
  }

  /**
   * Set the administrative title of the atom.
   *
   * @param string $title
   *   The administrative title of the atom.
   *
   * @return $this
   */
  public function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * Returns a render array representation of the object.
   *
   * @return array
   *   A render array.
   */
  public function toRenderable() {
    return $this->view()->toRenderable();
  }

  /**
   * Returns a ViewableAtom object for the atom.
   *
   * @return \Drupal\atoms\ViewableAtom
   *   The ViewableAtom object for the atom.
   */
  public function view() {
    /** @var AtomsViewBuilder $service */
    $service = \Drupal::service('atoms');
    return $service->get($this->getMachineName(), $this->getLangcode());
  }

  /**
   * Get the machine name of the atom.
   *
   * @return string|null
   *   The machine name of the atom.
   */
  public function getMachineName(): ?string {
    return $this->machineName;
  }

  /**
   * Set the machine name of the atom.
   *
   * @param string $machineName
   *   The machine name of the atom.
   *
   * @return $this
   */
  public function setMachineName($machineName) {
    $this->machineName = $machineName;
    return $this;
  }

  /**
   * Get the language code of the atom.
   *
   * @return string
   *   The language code of the atom.
   */
  public function getLangcode() {

    if (empty($this->langcode)) {
      return LanguageInterface::LANGCODE_NOT_SPECIFIED;
    }
    elseif (!$this->isTranslatable()) {
      return LanguageInterface::LANGCODE_NOT_APPLICABLE;
    }

    return $this->langcode;
  }

  /**
   * Set the language code of the atom.
   *
   * @param string $langcode
   *   The language code of the atom.
   *
   * @return $this
   */
  public function setLangcode($langcode) {
    if ($this->isTranslatable()) {
      $this->langcode = $langcode;
    }
    else {
      $this->langcode = LanguageInterface::LANGCODE_NOT_APPLICABLE;
    }
    return $this;
  }

  /**
   * Returns the translation support status.
   *
   * @return bool
   *   TRUE if the object has translation support enabled.
   */
  public function isTranslatable() {
    return $this->getOptions()['translatable'] ?? TRUE;
  }

  /**
   * Get the options for the atom.
   *
   * @return array
   *   The options for the atom.
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * Set the options for the atom.
   *
   * @param array $options
   *   The options for the atom.
   *
   * @return $this
   */
  public function setOptions(array $options) {
    $this->options = $options;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    if (!$this->isTranslatable()) {
      return [];
    }
    else {
      return [
        'languages:' . LanguageInterface::TYPE_CONTENT,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [
      'atoms:' . $this->getMachineName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function language() {
    \Drupal::languageManager()->getLanguage($this->getLangcode());
  }

  /**
   * {@inheritdoc}
   */
  public function isNewTranslation() {
    return $this->isNew();
  }

  /**
   * Return whether the atom is new.
   *
   * @return bool
   *   TRUE if the atom is new.
   */
  public function isNew() {
    return $this->isNew;
  }

  /**
   * Set whether the Atom object is new and previously unsaved to the database.
   *
   * @param bool $isNew
   *   Set to TRUE if the atom is new.
   */
  public function setIsNew(bool $isNew) {
    $this->isNew = $isNew;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationLanguages($include_default = TRUE) {
    $translations = $this->getStorage()->getTranslations($this->getMachineName());
    if (!$include_default) {
      foreach ($translations as $index => $langcode) {
        if ($langcode == $this->getDefaultLangcode()) {
          unset($translations[$index]);
        }
      }
    }
    return $translations;
  }

  /**
   * Get the default language code.
   *
   * @return string
   *   The default language code.
   */
  protected function getDefaultLangcode() {
    return \Drupal::languageManager()->getDefaultLanguage()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function getUntranslated() {
    if ($this->isDefaultTranslation()) {
      return $this;
    }
    else {
      return $this->getTranslation($this->getDefaultLangcode());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultTranslation() {
    return $this->getLangcode() == $this->getDefaultLangcode();
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslation($langcode) {
    return static::load($this->getMachineName(), $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function addTranslation($langcode, array $values = []) {
    if ($this->hasTranslation($langcode)) {
      throw new \InvalidArgumentException();
    }
    $atom = clone $this;
    $atom->setLangcode($langcode);
    if (!empty($values)) {
      $atom->setData($values);
    }
    try {
      $atom->save();
      return $atom;
    }
    catch (\Exception) {
      return $atom;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasTranslation($langcode) {
    return $this->getStorage()->languageExists($this->getMachineName(), $langcode);
  }

  /**
   * Save the atom to storage.
   *
   * @return $this
   *
   * @throws \Drupal\atoms\AtomsStorageException
   */
  public function save() {
    if ($storage = $this->getStorage()) {
      $storage->save($this);
    }
    else {
      throw new AtomsStorageException('The data cannot be saved because its not bound to a storage: ' . $this->getMachineName());
    }
    return $this;
  }

  /**
   * Removes the translation identified by the given language code.
   *
   * @param string $langcode
   *   The language code identifying the translation to be removed.
   */
  public function removeTranslation($langcode) {
    if ($langcode != $this->getDefaultLangcode()) {
      $this->getStorage()
        ->deleteTranslation($this->getMachineName(), $langcode);
    }
  }

  /**
   * Load an atom using its machine name.
   *
   * @param string $machineName
   *   The machine name of the atom to load.
   * @param string|null $langcode
   *   (optional) The language code to load the atom with.
   *
   * @return \Drupal\atoms\Atom|null
   *   The atom or NULL if not found.
   */
  public static function load($machineName, $langcode = NULL) {
    $atom = self::loadMultiple([$machineName], $langcode);
    if (empty($atom)) {
      return NULL;
    }
    else {
      return reset($atom);
    }
  }

  /**
   * Load entire group of atoms.
   *
   * @param string $groupId
   *   The group ID to load.
   * @param string|null $langcode
   *   Optional language code to load, defaults to current language.
   *
   * @return Atom[]
   *   Atoms belonging to the group.
   */
  public static function loadGroup($groupId, $langcode = NULL) {
    /** @var AtomsStorage $storage */
    $storage = \Drupal::service('atoms.storage');
    $machine_names = $storage->getMachineNames($groupId);
    return self::loadMultiple($machine_names, $langcode);
  }

  /**
   * Loads one or more atoms.
   *
   * @param array $machineNames
   *   Array of machine names to load.
   * @param string|null $langcode
   *   Optional language code to load, defaults to current language.
   *
   * @return Atom[]
   *   An array of atoms indexed by their machine names.
   */
  public static function loadMultiple(array $machineNames, $langcode = NULL) {
    /** @var AtomsStorage $storage */
    $storage = \Drupal::service('atoms.storage');
    $loaded = $storage->loadMultiple($machineNames, $langcode);

    $response = [];
    foreach ($loaded as $content) {
      $response[] = new Atom($content);
    }
    return $response;
  }

  /**
   * Returns a render array representation of the object.
   *
   * @param string $machineName
   *   The machine name of the atom to render.
   * @param string|null $langcode
   *   (optional) The language code to render the atom with.
   *
   * @return array
   *   A render array to render the atom.
   */
  public static function get($machineName, $langcode = NULL) {
    return self::getView($machineName, $langcode)->toRenderable();
  }

  /**
   * Returns a render array representation of the object.
   *
   * @param string $machineName
   *   The machine name of the atom to render.
   * @param string|null $langcode
   *   (optional) The language code to render the atom with.
   *
   * @return array
   *   A render array to render the atom.
   */
  public static function getAsRenderable($machineName, $langcode = NULL) {
    return self::getView($machineName, $langcode)->toRenderable();
  }

  /**
   * Returns a rendered string version of the object.
   *
   * @param string $machine_name
   *   The machine name of the atom to render.
   * @param string|null $langcode
   *   (optional) The language code to render the atom with.
   *
   * @return string
   *   Atom rendered as a plain string.
   */
  public static function getAsString($machine_name, $langcode = NULL) {
    return self::getView($machine_name, $langcode)->toString();
  }

  /**
   * Returns a ViewableAtom.
   *
   * @param string $machineName
   *   The machine name of the atom to view.
   * @param string|null $langcode
   *   (optional) The language code to view the atom with.
   *
   * @return \Drupal\atoms\ViewableAtom
   *   The ViewableAtom object.
   */
  public static function getView($machineName, $langcode = NULL) {
    /** @var \Drupal\atoms\AtomsViewBuilder $service */
    static $service = NULL;
    if (is_null($service)) {
      $service = \Drupal::service('atoms');
    }
    return $service->get($machineName, $langcode);
  }

  /**
   * Returns a lazy builder version of the object.
   *
   * @param string $machineName
   *   The machine name of the atom to render.
   * @param string|null $langcode
   *   (optional) The language code to render the atom with.
   *
   * @return array
   *   Render array for atom through a lazy builder.
   */
  public static function getLazyBuilder($machineName, $langcode = NULL) {
    /** @var \Drupal\atoms\AtomsViewBuilder $service */
    static $service = NULL;
    if (is_null($service)) {
      $service = \Drupal::service('atoms');
    }
    return $service->getLazyBuilder($machineName, $langcode);
  }

  /**
   * Applies CacheableMetadata from list of Atoms to render array.
   *
   * @param array $build
   *   Render array that depend on one or more atoms.
   * @param string[] $machineNames
   *   Array of machine names for the atoms used in the render array.
   * @param bool $add_contextual_links
   *   (optional) Should contextual links be added as well?
   * @param string|null $langcode
   *   (optional) The language code to render the atom with.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   CacheableMetadata object with cache data for all the atoms.
   */
  public static function mergeCache(array &$build, array $machineNames, $add_contextual_links = FALSE, $langcode = NULL) {
    $cacheableMetadata = CacheableMetadata::createFromRenderArray($build);
    foreach ($machineNames as $machineName) {
      $cacheableMetadata = $cacheableMetadata->merge(self::getView($machineName, $langcode)->getCacheableMetadata());
    }
    if ($add_contextual_links && !empty($machineNames)) {
      if (empty($build['#contextual_links'])) {
        $build['#contextual_links'] = [];
      }
      $build['#contextual_links']['atoms'] = [
        'route_parameters' => ['group_id' => self::getView($machineNames[0], $langcode)->getAtom()->getGroupId()],
      ];
    }
    $cacheableMetadata->applyTo($build);
    return $cacheableMetadata;
  }

}
