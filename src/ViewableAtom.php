<?php

namespace Drupal\atoms;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\RenderableInterface;

/**
 * Atom ready for viewing.
 */
class ViewableAtom implements RenderableInterface, CacheableDependencyInterface {

  use DependencySerializationTrait;

  /**
   * The machine name of the atom.
   *
   * @var string
   */
  protected $machineName = '';

  /**
   * The language code.
   *
   * @var string
   */
  protected $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED;

  /**
   * The plugin for the atom type.
   *
   * @var AtomsPluginInterface
   */
  protected $plugin;

  /**
   * The atom type.
   *
   * @var string
   */
  protected $type = '';

  /**
   * The atom to be viewed.
   *
   * @var Atom
   */
  protected $atom;

  /**
   * Render array of the atom.
   *
   * @var array
   */
  protected $rendered;

  /**
   * Options for viewing the atom.
   *
   * @var array
   */
  protected $options = [];

  /**
   * Token data for viewing the atom.
   *
   * @var array
   */
  protected $tokenData = [];

  /**
   * CacheableMetadata of the atom.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata */
  protected $cacheabilityMetadata;

  /**
   * ViewableAtom constructor.
   *
   * @param string $machineName
   *   The machine name of the atom to view.
   * @param string|null $langcode
   *   (optional) The language code to view the atom with.
   */
  public function __construct($machineName, $langcode = NULL) {
    $this->langcode = $langcode;
    $this->atom = Atom::load($machineName, $langcode);
    if (!empty($this->atom)) {
      $this->type = $this->atom->getType();
      $this->options = $this->atom->getOptions();
      $this->plugin = $this->atom->getPlugin();
    }
  }

  /**
   * Get the type of the atom.
   *
   * @return string
   *   The type.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get the options of the atom.
   *
   * @return array
   *   The options.
   */
  public function getOptions() {
    if (empty($this->getAtom())) {
      return [];
    }
    return $this->getAtom()->getOptions();
  }

  /**
   * Set the options of the atom.
   *
   * @param array $options
   *   The options to set.
   *
   * @return $this
   */
  public function setOptions(array $options) {
    $this->getAtom()->setOptions($options);
    $this->rendered = NULL;
    return $this;
  }

  /**
   * Get the atom.
   *
   * @return Atom
   *   The atom.
   */
  public function getAtom() {
    if (empty($this->atom)) {
      $this->atom = Atom::load($this->machineName, $this->langcode);
    }
    return $this->atom;
  }

  /**
   * Get the value of the atom.
   *
   * @return mixed
   *   The value of the atom.
   */
  public function getData() {
    if (empty($this->getAtom())) {
      return [];
    }
    return $this->getAtom()->getData();
  }

  /**
   * Get the language code of the atom.
   *
   * @return string
   *   The language code.
   */
  public function getLangCode() {
    if (empty($this->langcode)) {
      $this->langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }
    return $this->langcode;
  }

  /**
   * Set an option for an atom.
   *
   * @param string $key
   *   The option to be set.
   * @param mixed $value
   *   The value to set the option to.
   *
   * @return $this
   */
  public function setOption($key, $value) {
    $options = $this->getAtom()->getOptions();
    $options[$key] = $value;
    return $this->setOptions($options);
  }

  /**
   * Get the token data of the atom.
   *
   * @return array
   *   The token data.
   */
  public function getTokenData() {
    return $this->tokenData;
  }

  /**
   * Set the token data needed for the atom.
   *
   * @param array $data
   *   The token data.
   *
   * @return $this
   */
  public function setTokenData(array $data) {
    $this->tokenData = $data;
    $this->rendered = NULL;
    return $this;
  }

  /**
   * Get the render array of the atom.
   *
   * @return array
   *   A render array.
   */
  private function getRendered() {
    if (empty($this->rendered)) {
      if (!empty($this->plugin)) {
        $this->rendered = $this->plugin->render($this);
      }
      if (empty($this->rendered)) {
        $this->rendered = $this->toEmptyRenderable();
      }
    }
    return $this->rendered;
  }

  /**
   * Get a render array for empty atoms.
   *
   * @return array
   *   A render array.
   */
  private function toEmptyRenderable() {
    return ['#plain_text' => $this->machineName];
  }

  /**
   * Get the CacheableMetadata of an atom.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   The CacheableMetadata of an atom.
   */
  public function getCacheableMetadata() {
    if (empty($this->cacheabilityMetadata)) {
      $this->cacheabilityMetadata = CacheableMetadata::createFromRenderArray($this->getRendered());
      $this->cacheabilityMetadata->addCacheableDependency($this->getAtom());
    }
    return $this->cacheabilityMetadata;
  }

  /**
   * Returns a render array representation of the object.
   *
   * @return array
   *   A render array.
   */
  public function toRenderable() {
    $build = [
      '#theme' => 'atom',
      '#atom' => $this->getAtom(),
    ];
    $build[] = $this->getRendered();
    $this->getCacheableMetadata()->applyTo($build);
    $build['#theme_wrappers'] = ['atom_element'];
    if (!empty($this->getAtom())) {
      $build['#contextual_links'] = [
        'atoms' => [
          'route_parameters' => ['group_id' => $this->getAtom()->getGroupId()],
        ],
      ];
    }
    return $build;
  }

  /**
   * Get a string representation of an atom.
   *
   * @return string
   *   The atom represented as a plain string.
   */
  public function __toString() {
    /** @var \Drupal\Core\Render\Renderer $renderer */
    static $renderer = NULL;
    if (is_null($renderer)) {
      $renderer = \Drupal::service('renderer');
    }
    $rendered = $this->getRendered();
    $string = $renderer->renderInIsolation($rendered);
    if (is_string($string)) {
      return $string;
    }
    else {
      return $string->__toString();
    }
  }

  /**
   * Get a string representation of an atom.
   *
   * @return string
   *   The atom represented as a plain string.
   */
  public function toString() {
    return $this->__toString();
  }

  /**
   * Get a value from an atom.
   *
   * @param string $key
   *   The specific value wanted in case the atom has multiple.
   *
   * @return mixed
   *   The value of an atom.
   */
  public function toValue($key = '') {
    return $this->getAtom()->getPlugin()->value($this, $key);
  }

  /**
   * Get all values of an atom.
   *
   * @return array
   *   All values of the atom.
   */
  public function getValues() {
    return $this->getAtom()->getPlugin()->values($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return $this->getCacheableMetadata()->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->getCacheableMetadata()->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return $this->getCacheableMetadata()->getCacheMaxAge();
  }

}
