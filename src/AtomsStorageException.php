<?php

namespace Drupal\atoms;

/**
 * Storage exception for atoms storage.
 */
class AtomsStorageException extends \Exception {
}
