<?php

namespace Drupal\atoms\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an import annotation object.
 *
 * Plugin Namespace: Plugin\Atoms.
 *
 * @see \Drupal\Core\ArchiverImportrManager
 * @see \Drupal\Core\Archiver\ImportInterface
 * @see plugin_api
 * @see hook_import_alter()
 *
 * @Annotation
 */
class Atoms extends Plugin {

  /**
   * The import plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The field types of the plugin.
   *
   * @var array
   */
  public $types;

}
