<?php

namespace Drupal\atoms;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for atom plugins.
 */
abstract class AtomsPluginBase extends PluginBase implements AtomsPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * BubbleableMetadata for the atom.
   *
   * @var \Drupal\Core\Render\BubbleableMetadata
   */
  protected $bubbleableMetadata;

  /**
   * The atoms settings configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The token utility.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Construct AtomsPluginBase object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token utility.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The atoms settings configuration.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, Token $token, ImmutableConfig $config, array $configuration, $plugin_id, $plugin_definition) {
    $this->config = $config;
    $this->moduleHandler = $moduleHandler;
    $this->token = $token;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $configFactory = $container->get('config.factory');
    $config = $configFactory->get('atoms.settings');
    $moduleHandler = $container->get('module_handler');
    $token = $container->get('token');
    return new static($moduleHandler, $token, $config, $configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function validate(&$form, FormStateInterface $form_state) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(Atom $atom, FormStateInterface $form_state) {
    $atom->setData($form_state->getValue($this->getFormStateKey($atom)));
  }

  /**
   * Find the form state key for an atom.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom to get the form state key for.
   *
   * @return array
   *   The form state key for the current atom.
   */
  protected function getFormStateKey(Atom $atom) {
    if (empty($atom->getSubgroupName())) {
      $subgroup = '0';
    }
    else {
      $subgroup = $atom->getSubgroupId();
    }
    return [$subgroup, $atom->getMachineName()];
  }

  /**
   * {@inheritdoc}
   */
  public function values(ViewableAtom $view) {
    return [
      'value' => $this->value($view),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function value(ViewableAtom $view, $key = '') {
    /** @var \Drupal\Core\Render\Renderer $renderer */
    static $renderer = NULL;
    if (is_null($renderer)) {
      $renderer = \Drupal::service('renderer');
    }
    $build = $this->render($view);
    return $renderer->renderInIsolation($build);
  }

  /**
   * {@inheritDoc}
   */
  public function render(ViewableAtom $view) {
    $this->bubbleableMetadata = new BubbleableMetadata();
    $build = [];
    $this->renderBuild($view, $build);
    $this->bubbleableMetadata->applyTo($build);
    return $build;
  }

  /**
   * Return a render array for an atom of this type.
   *
   * @param \Drupal\atoms\ViewableAtom $view
   *   The viewable atom to render.
   * @param array $build
   *   The render array to insert into.
   */
  abstract public function renderBuild(ViewableAtom $view, array &$build);

  /**
   * {@inheritdoc}
   */
  public function settingsForm(Config $config, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsValidate(Config $config, FormStateInterface $form_state) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSubmit(Config $config, FormStateInterface $form_state) {

  }

  /**
   * Utility function to prepare values if there is token support.
   *
   * @param \Drupal\atoms\ViewableAtom $view
   *   The viewable atom to prepare a value for.
   * @param string $text
   *   The text that needs to be prepared.
   *
   * @return string
   *   Fully prepared text value.
   */
  protected function prepareText(ViewableAtom $view, $text) {
    if ($this->isTokenSupportEnabled()) {
      return $this->token
        ->replace($text, $view->getTokenData(), [], $this->bubbleableMetadata);
    }
    else {
      return $text;
    }
  }

  /**
   * Whether the plugin supports tokens or not.
   *
   * @return bool
   *   Whether the plugin supports tokens or not.
   */
  protected function isTokenSupportEnabled() {
    if ($this->moduleHandler
      ->moduleExists('token') && $this->getConfig()->get('tokens')) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the atoms settings configuration.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The atoms settings configuration.
   */
  protected function getConfig() {
    return $this->config;
  }

}
