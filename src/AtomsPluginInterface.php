<?php

namespace Drupal\atoms;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for atoms plugins.
 */
interface AtomsPluginInterface extends PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * Return a list of content types for use in the administration interface.
   *
   * @return array
   *   List of names of content types.
   */
  public function getTypeNames();

  /**
   * Tells whether the plugin supports tokens or not.
   *
   * @return bool
   *   Return TRUE if the plugin supports tokens and FALSE otherwise.
   */
  public function hasTokenSupport();

  /**
   * Get a value from an atom.
   *
   * @param ViewableAtom $view
   *   The viewable atom you want the value of.
   * @param string $key
   *   The specific value of the atom you want if it has multiple.
   *
   * @return mixed
   *   The value of the atom.
   */
  public function value(ViewableAtom $view, $key = '');

  /**
   * Get all the value from an atom.
   *
   * @param ViewableAtom $view
   *   The viewable atom you want the values of.
   *
   * @return array
   *   An array of values indexed by key.
   */
  public function values(ViewableAtom $view);

  /**
   * Get a render array of an atom.
   *
   * @param ViewableAtom $view
   *   The viewable atom you want the render of.
   *
   * @return array
   *   A render array to render the atom type.
   */
  public function render(ViewableAtom $view);

  /**
   * Get a summary of an atom for administrative purposes.
   *
   * @param Atom $atom
   *   The viewable atom you want a summary of.
   *
   * @return array
   *   A render array to render a summary of the atom type.
   */
  public function summary(Atom $atom);

  /**
   * Get a form for editing an atom of this type.
   *
   * This if not a full form as it's often grouped with
   * other atoms.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom to build an edit form for.
   *
   * @return array
   *   Form array for editing the atom.
   */
  public function formBuilder(Atom $atom);

  /**
   * Validate the values for editing an atom of this type.
   *
   * @param array $form
   *   The form array used to edit the atom.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Returns TRUE if the values are valid otherwise FALSE.
   */
  public function validate(&$form, FormStateInterface $form_state);

  /**
   * Save the values that was edited of an atom of this type.
   *
   * @param \Drupal\atoms\Atom $atom
   *   The atom being edited.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submit(Atom $atom, FormStateInterface $form_state);

  /**
   * Get a form for editing settings for this atom type.
   *
   * @param \Drupal\Core\Config\Config $config
   *   Configuration where settings are saved for this atom type.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form array of settings for this atom type.
   */
  public function settingsForm(Config $config, FormStateInterface $form_state);

  /**
   * Validate settings for this atom type.
   *
   * @param \Drupal\Core\Config\Config $config
   *   Configuration where settings are saved for this atom type.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Returns TRUE if the settings are valid otherwise FALSE.
   */
  public function settingsValidate(Config $config, FormStateInterface $form_state);

  /**
   * Save settings for this atom type in the config.
   *
   * @param \Drupal\Core\Config\Config $config
   *   Configuration where settings are saved for this atom type.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function settingsSubmit(Config $config, FormStateInterface $form_state);

}
