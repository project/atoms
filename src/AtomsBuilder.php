<?php

namespace Drupal\atoms;

use Drupal\Component\Discovery\YamlDiscovery;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Service for rebuilding atoms during cache rebuild.
 */
class AtomsBuilder {

  /**
   * Atom plugin definitions based on type.
   *
   * @var array
   */
  static protected $types;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;
  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * The atoms plugin manager.
   *
   * @var AtomsManager
   */
  protected $manager;
  /**
   * The atoms storage service.
   *
   * @var AtomsStorage
   */
  protected $storage;
  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs the AtomsBuilder.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param AtomsManager $manager
   *   The atoms plugin manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger factory for getting logger.
   * @param AtomsStorage $storage
   *   The atoms storage service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ThemeHandlerInterface $themeHandler, Connection $connection, AtomsManager $manager, LoggerChannelFactoryInterface $loggerChannelFactory, AtomsStorage $storage, LanguageManagerInterface $languageManager) {
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $themeHandler;
    $this->connection = $connection;
    $this->manager = $manager;
    $this->storage = $storage;
    $this->logger = $loggerChannelFactory->get('atoms');
    $this->languageManager = $languageManager;
  }

  /**
   * Rebuild the data in the database based on yaml files.
   *
   * @throws \Drupal\atoms\AtomsStorageException
   */
  public function rebuild() {
    $new_names = [];
    $machine_names = $this->storage->getMachineNames();
    $definitions = $this->getAtomsDefinitions();

    foreach ($definitions as $items) {
      $atoms = $items['atoms'] ?? [];
      foreach ($atoms as $machine_name => $data) {
        if (!$this->typeExists($data['type'])) {
          continue;
        }

        if (empty($data['category'])) {
          $data['category'] = 'Miscellaneous';
        }
        if (empty($data['group'])) {
          $data['group'] = $data['category'];
          $data['category'] = 'Miscellaneous';
          if (isset($data['subcategory'])) {
            $data['subgroup'] = 'subcategory';
            unset($data['subcategory']);
          }
        }
        if (empty($data['description'])) {
          $data['description'] = '';
        }

        $new_names[] = $machine_name;
        $atom = $this->storage->createAtom([
          'machineName' => $machine_name,
          'type' => $data['type'],
          'title' => $data['title'],
          'description' => $data['description'],
          'category' => $data['category'],
          'groupName' => $data['group'],
        ]);

        if (in_array($machine_name, $machine_names)) {
          $atom->setIsNew(FALSE);
        }
        else {
          $machine_names[] = $machine_name;
        }

        if (isset($data['default'])) {
          $atom->setDefault($data['default']);
        }
        else {
          $atom->setDefault([
            $this->languageManager->getDefaultLanguage()->getId() => '',
          ]);
        }

        if (empty($data['langcode'])) {
          $data['langcode'] = $this->languageManager->getDefaultLanguage()->getId();
        }

        if (isset($data['langcode'])) {
          $atom->setLangcode($data['langcode']);
        }

        if (isset($data['options']) && is_array($data['options'])) {
          $atom->setOptions($data['options']);
        }

        if (isset($data['subgroup'])) {
          $atom->setSubgroupName($data['subgroup']);
        }

        if (isset($data['weight'])) {
          $atom->setWeight(intval($data['weight']));
        }

        $atom->rebuild();
      }
    }

    $forgotten_names = array_diff($machine_names, $new_names);
    if (!empty($forgotten_names)) {
      foreach ($forgotten_names as $machine_name) {
        $this->storage->delete($machine_name);
      }
    }
  }

  /**
   * Retrieves all defined routes from .routing.yml files.
   *
   * @return array
   *   The defined routes, keyed by provider.
   */
  protected function getAtomsDefinitions() {
    // Always instantiate a new YamlDiscovery object so that we always search on
    // the up-to-date list of modules.
    $directories = array_merge($this->moduleHandler->getModuleDirectories(), $this->themeHandler->getThemeDirectories());
    $discovery = new YamlDiscovery('atoms', $directories);
    $definitions = $discovery->findAll();

    $this->moduleHandler->alter('atoms', $definitions);

    return $definitions;
  }

  /**
   * Return whether an atom type exists.
   *
   * @return bool
   *   Whether an atom type exists.
   */
  protected function typeExists($type) {
    if (empty(self::$types)) {
      $definitions = $this->manager->getDefinitions();
      foreach ($definitions as $key => $definition) {
        foreach ($definition['types'] as $type) {
          self::$types[$type] = $key;
        }
      }
    }

    return isset(self::$types[$type]);
  }

}
