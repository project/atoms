<?php

/**
 * @file
 * Hooks related to the Atoms module.
 */

/**
 * Alter the atoms definitions.
 *
 * @param array $definitions
 *   The atoms definitions.
 */
function hook_atoms_alter(&$definitions) {

}
