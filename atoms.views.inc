<?php

/**
 * @file
 * Provide views data for atoms.
 */

/**
 * Implements hook_views_data_alter().
 */
function atoms_views_data_alter(array &$data) {
  $data['views']['atoms'] = [
    'title' => t('Atoms'),
    'help' => t('Atomic content that can be used globally.'),
    'area' => [
      'id' => 'atoms',
    ],
  ];
}
