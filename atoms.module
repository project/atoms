<?php

/**
 * @file
 * Add atomic content pieces that can be used globally.
 */

use Drupal\atoms\Atom;
use Drupal\atoms\AtomsStorageException;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_modules_installed().
 *
 * Make sure to include any new atoms the new module may have.
 */
function atoms_modules_installed($modules) {
  /** @var \Drupal\atoms\AtomsBuilder $builder */
  $builder = \Drupal::service('atoms.builder');
  try {
    $builder->rebuild();
  }
  catch (AtomsStorageException) {
    \Drupal::logger('atoms')->error('Error when trying to rebuild atoms.');
  }
}

/**
 * Implements hook_theme().
 */
function atoms_theme() {
  return [
    'atom' => [
      'render element' => 'elements',
    ],
    'atom_element' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Implements hook_help().
 */
function atoms_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the atoms module.
    case 'help.page.atoms':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Add atomic sized content to your site.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Adding new atoms') . '</dt>';
      $output .= '<dd>' . t('New atoms must be added through yaml files or hooks.') . '</dd>';
      $output .= '<dt>' . t('Support for Media Library') . '</dt>';
      $output .= '<dd>' . t('To be able to use the <em>Media Library</em> in atoms you must first install the <em>Media Library Form Element</em> module and then you can activate the <em>Atoms Media Library</em> module.') . '</dd>';
      $output .= '<dt>' . t('Configuring plugins') . '</dt>';
      $output .= '<dd>' . t('Default options for all <em>atom types</em> can be configured on the <a href=":settings">Atoms settings page</a>.', [':settings' => Url::fromRoute('atoms.settings')->toString()]) . '</dd>';
      $output .= '<dt>' . t('Adding token support') . '</dt>';
      $output .= '<dd>' . t('Token support for the <em>atom types</em> that support it can be activated on the <a href=":settings">Atoms settings page</a>.', [':settings' => Url::fromRoute('atoms.settings')->toString()]) . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implements hook_rebuild().
 */
function atoms_rebuild() {
  /** @var \Drupal\atoms\AtomsManager $manager */
  $manager = \Drupal::service('plugin.atoms.manager');
  $manager->setCachedAtomTypes();

  /** @var \Drupal\atoms\AtomsBuilder $builder */
  $builder = \Drupal::service('atoms.builder');
  try {
    $builder->rebuild();
  }
  catch (AtomsStorageException) {
    \Drupal::logger('atoms')->error('Error when trying to rebuild atoms.');
  }
}

/**
 * Returns a render array representation of the object.
 *
 * @param string $machine_name
 *   The machine name of the atom to render.
 * @param string|null $langcode
 *   (optional) The language code to render the atom with.
 *
 * @return array
 *   A render array to render the atom.
 *
 * @deprecated in atoms:1.0.0 and is removed from atoms:2.0.0. Use
 *    Atom::get instead.
 *
 * @see https://www.drupal.org/node/3398085
 */
function atom($machine_name, $langcode = NULL) {
  return atom_view($machine_name, $langcode)->toRenderable();
}

/**
 * Returns a rendered string version of the object.
 *
 * @param string $machine_name
 *   The machine name of the atom to render.
 * @param string|null $langcode
 *   (optional) The language code to render the atom with.
 *
 * @return string
 *   Atom rendered as a plain string.
 *
 * @deprecated in atoms:1.0.0 and is removed from atoms:2.0.0. Use
 *   Atom::getAsString instead.
 *
 * @see https://www.drupal.org/node/3398085
 */
function atom_str($machine_name, $langcode = NULL) {
  return atom_view($machine_name, $langcode)->toString();
}

/**
 * Returns a ViewableAtom.
 *
 * @param string $machine_name
 *   The machine name of the atom to view.
 * @param string|null $langcode
 *   (optional) The language code to render the atom with.
 *
 * @return \Drupal\atoms\ViewableAtom
 *   The ViewableAtom object.
 *
 * @deprecated in atoms:1.0.0 and is removed from atoms:2.0.0. Use
 *  Atom::getView instead.
 *
 * @see https://www.drupal.org/node/3398085
 */
function atom_view($machine_name, $langcode = NULL) {
  /** @var \Drupal\atoms\AtomsViewBuilder $service */
  static $service = NULL;
  if (is_null($service)) {
    $service = \Drupal::service('atoms');
  }
  return $service->get($machine_name, $langcode);
}

/**
 * Applies CacheableMetadata from list of Atoms to render array.
 *
 * @param array $build
 *   Render array that depend on one or more atoms.
 * @param string[] $machine_names
 *   Array of machine names for the atoms used in the render array.
 * @param bool $add_contextual_links
 *   (optional) Should contextual links be added as well?
 * @param string|null $langcode
 *   (optional) The language code to render the atom with.
 *
 * @deprecated in atoms:1.0.0 and is removed from atoms:2.0.0. Use
 *   Atom::mergeCache instead.
 *
 * @see https://www.drupal.org/node/3398085
 */
function atom_cache(array &$build, array $machine_names, $add_contextual_links = FALSE, $langcode = NULL) {
  $cacheableMetadata = CacheableMetadata::createFromRenderArray($build);
  foreach ($machine_names as $machine_name) {
    $cacheableMetadata = $cacheableMetadata->merge(atom_view($machine_name, $langcode)->getCacheableMetadata());
  }
  if ($add_contextual_links && !empty($machine_names)) {
    if (empty($build['#contextual_links'])) {
      $build['#contextual_links'] = [];
    }
    $build['#contextual_links']['atoms'] = [
      'route_parameters' => ['group_id' => atom_view($machine_names[0], $langcode)->getAtom()->getGroupID()],
    ];
  }
  $cacheableMetadata->applyTo($build);
  return $cacheableMetadata;
}

/**
 * Returns a lazy builder version of the object.
 *
 * @param string $machine_name
 *   The machine name of the atom to render.
 * @param string|null $langcode
 *   (optional) The language code to render the atom with.
 *
 * @return array
 *   Render array for atom through a lazy builder.
 *
 * @deprecated in atoms:1.0.0 and is removed from atoms:2.0.0. Use
 *   Atom::getLazyBuilder instead.
 *
 * @see https://www.drupal.org/node/3398085
 */
function atom_lazy($machine_name, $langcode = NULL) {
  /** @var \Drupal\atoms\AtomsViewBuilder $service */
  $service = \Drupal::service('atoms');
  return $service->getLazyBuilder($machine_name, $langcode);
}

/**
 * Preprocess function for the atom template.
 *
 * @param array $variables
 *   An associative array containing:
 *   - atom: The atom object itself.
 *   - type: The type of atom.
 *   - data: The data the atom includes.
 */
function template_preprocess_atom(&$variables) {
  /** @var \Drupal\atoms\Atom $atom */
  $atom = $variables['elements']['#atom'];
  if (!empty($atom)) {
    $variables['atom'] = $atom;
    $variables['type'] = $atom->getType();
    $variables['data'] = $atom->getData();
  }

  // Helpful $content variable for templates.
  $variables += ['content' => []];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Implements hook_contextual_links_view_alter().
 *
 * Change Configure Blocks into off_canvas links.
 */
function atoms_contextual_links_view_alter(&$element, $items) {

  if (isset($element['#links']['settings-tray-atoms-configure'])) {
    // Place settings_tray link first.
    $settings_tray_link = $element['#links']['settings-tray-atoms-configure'];
    unset($element['#links']['settings-tray-atoms-configure']);
    $element['#links'] = ['settings-tray-atoms-configure' => $settings_tray_link] + $element['#links'];

    $element['#attached']['library'][] = 'core/drupal.dialog.off_canvas';
  }
}

/**
 * Implements hook_token_info().
 */
function atoms_token_info() {
  $types = [
    // [atoms:]
    'atoms' => [
      'name' => t('Atoms'),
      'description' => t('Tokens for atoms.'),
    ],
  ];

  $tokens = [
    // [atoms:]
    'atoms' => [],
  ];
  /** @var \Drupal\atoms\AtomsStorage $storage */
  $storage = \Drupal::service('atoms.storage');
  $machine_names = $storage->getMachineNames();
  $atoms = Atom::loadMultiple($machine_names);
  foreach ($atoms as $atom) {
    $tokens['atoms'][$atom->getMachineName()] = [
      'name' => $atom->getTitle(),
      'description' => t(':category -> :groupName -> :title',
        [
          ':category' => $atom->getCategory(),
          ':groupName' => $atom->getGroupName(),
          ':title' => $atom->getTitle(),
        ]
      ),
    ];
  }

  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function atoms_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $langcode = $options['langcode'] ?? NULL;
  $replacements = [];
  if ($type == 'atoms') {
    foreach ($tokens as $machine_name => $original) {
      $viewable = Atom::getView($machine_name, $langcode);
      $replacements[$original] = $viewable->toString();
      $bubbleable_metadata->addCacheableDependency($viewable);
    }
  }
  return $replacements;
}

/**
 * Massage atom transforms.
 *
 * @param array $transformation
 *   The transform to be massaged.
 */
function atoms_atom_transform_alter(&$transformation) {
  switch ($transformation['atom_type']) {
    case 'button':
    case 'link':
      try {
        $url = Url::fromUri($transformation['uri'] ?? '');
      }
      catch (InvalidArgumentException) {
        \Drupal::logger('atoms')
          ->error('Atom (' . $transformation['machine_name'] . ') of type ' . $transformation['atom_type'] . ' has invalid data');
        $url = Url::fromUri('internal:/');
      }
      $transformation['url'] = $url->toString();
      break;
  }
}
