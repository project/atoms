# Atoms #

## Introduction

Atoms is meant to give developers an easy and quick way to add small pieces of content to a site that an administrator
can then freely change as they see fit. Of course there are many options for content already like nodes and other
entities to fill out a page, but these are usually overkill when just talking about a text in a custom form or similar.

This is what Atoms is for. Very small pieces of content in places where it's usually quite bothersome to embed any other
type of content entity.

## Usage

For most use cases using Atoms is as easy as calling the Atom::get() function whenever you need it.

```php
    $build = Atom::get('example-machine-name');
```

This returns a render array with all the necessary cacheability information and context menus for the Atom in question.
There are also other variants for more specialized uses like Atom::getLazyBuilder() which works like atom() but as a lazy builder
where performance might be an issue. Last we have Atom::getString() which just delivers a rendered string, but cacheability is
lost and must be taken care of manually.

There are also Twig functions called atom(), atomString() and atomLazy() with the same functionality.

```twig
    <p>{{ atom('example-machine-name') }}</p>
```

## Defining atoms

When defining new Atom you need to create a MODULE.atoms.yml file or THEME.atoms.yml which will include definitions for
all the atoms needed for that module or theme. All definition files must begin with atoms: as the first line and the
individual atoms beneath it.

```yaml
atoms:
  example-machine-name:
    type: textfield
    title: Example title
    description: An Atom used as an example for documentation.
    category: Example Category
    group: Example group
    subgroup: Example group
    default:
      en: 'English test title'
      da: 'Dansk test titel'
    options:
      translatable: TRUE
      token_types:
        - node
```

Every definition starts with a unique machine name for the Atom followed by the following properties:

* **type:** The type of field for this particular Atom. You can see a list of types further down includes any options
  they might have.
* **title:** The administration interface name for the Atom. This string is will be translated through t() so write it
  in english and provide a translation for it for other languages.
* **description:** (Optional) The administration interface description of the Atom. This string is also translated
  through t().
* **category:** (Optional) The category for administration interface to put the Atom in. Translated through t().
  Defaults to "Miscellaneous".
* **group:** The group for administration interface to organize it together with other Atom. If the same group name is
  used in multiple categories it will create separate groups. Translated through t().
* **subgroup:** (Optional) An optional further organization of the administration interface within the same group.
  Translated through t().
* **weight:** (Optional) An optional ordering of Atom within the same (sub)group of the administration interface.
* **default:** (Optional) A default value for the Atom so it will not be empty upon being created. Should be provided as
  an array with language codes as index making it possible to provide translations upon creation.
* **options:** (Optional) A field might need additional information depending upon type. This will be explored in detail
  under each field type. However two options are more universal:
  * **translatable:** (Optional) Whether the Atom can be translated. A phone number or e-mail might not need to
    change between languages and so will only have one version. Sets the language to "Not applicable" and default value
    should be provided with langcode "zxx". Defaults to TRUE.
  * **token_types:** (Optional) If the field type supports tokens and token support has been activated, the Atom will
    have access to global tokens. If you want to provide access to further token types these need to be specified here
    as an array. This will also require you to provide extra data for these token types. See more under Advanced Usage.

## Field Types

### textfield

* Tokens: yes
* Value: string

Provides a simple plain text field for entering short texts containing no markup.

### textarea

* Tokens: yes
* Value: string

Same as textfield, but for longer texts and provided with multiple lines for editing.

### email

* Tokens: yes
* Value: string

Same as textfield, but with validation for e-mail addresses.

### number

* Tokens: no
* Value: integer

Gives a field for entering a number integer with validation and support for mobile number input.

### text_format

* Tokens: yes
* Value: string
* Options:
  * format: (Optional) The text format initially used for this field. Defaults to the configured default setting.
  * allowed_formats: (Optional) An option to restrict the allowed text formats for this field. Defaults to the
    configured default setting.

The field gives access to a Rich Text editor enabling the administrator to use markup for formatting. The default text
format and allowed formats can be configured for all text_format fields on the Atoms settings page.

### date

* Tokens: no
* Value: DrupalDateTime
* Options:
  * format: (Optional) The date format used to display this field. Defaults to the configured default setting.

This field gives a date picker for selecting a date to be displayed in a selected Drupal date format. The default date
format can be configured for all date fields on the Atoms settings page.

### datetime

* Tokens: no
* Value: DrupalDateTime
* Options:
  * format: (Optional) The date format used to display this field. Defaults to the configured default setting.

This field gives a date picker for selecting a date and time to be displayed in a selected Drupal date format. The
default date format can be configured for all datetime fields on the Atoms settings page.

### url

* Tokens: yes
* Value: Url
* Options:
  * internal: (Optional) Whether the url can be an internal url on the site. Defaults to TRUE.
  * external: (Optional) Whether the url can be an external url on the world wide web. Defaults to TRUE.

The field allows the administrator to specify an url, either internal, external or both. If internal urls are allowed it
will give suggestions for nodes, but any relative url is allowed.

### link

* Tokens: yes
* Value:
  * title: string
  * url: Url
* Options:
  * internal: (Optional) Whether the url can be an internal url on the site. Defaults to TRUE.
  * external: (Optional) Whether the url can be an external url on the world wide web. Defaults to TRUE.

The field allows the administrator to specify an url, either internal, external or both and a title which together
creates a link. If internal urls are allowed it will give suggestions for nodes, but any relative url is allowed.

### button

* Tokens: yes
* Value:
  * title: string
  * url: Url
* Options:
  * internal: (Optional) Whether the url can be an internal url on the site. Defaults to TRUE.
  * external: (Optional) Whether the url can be an external url on the world wide web. Defaults to TRUE.

The field allows the administrator to specify an url, either internal, external or both and a title which together
creates a button. If internal urls are allowed it will give suggestions for nodes, but any relative url is allowed.
The field type default to a link with the button class when rendered, but can be configured to use any other class as
appropriate to the theme on the site.

### entity

* Tokens: no
* Value: Entity[]
* Options:
  * entity_type: (Optional) The type of entity to be selected. Defaults to "node".
  * multiple: (Optional) Whether you can select multiple entities. Defaults to FALSE.
  * target_bundles: (Optional) If you want to restrict the which bundles can be selected for the field. Defaults to all
    bundles.
  * view_mode: (Optional) Specifies what view mode the entities will be rendered with. Defaults to "full".
  * summary_view_mode: (Optional) Specifies what view mode the entities will be rendered with in the administration
    interface. Defaults to "teaser".

This field can allow the administrator to select a single or multiple entities of a certain type using an autocomplete
field.

### media (Separate module)

* Tokens: no
* Value: Media[]
* Options:
  * multiple: (Optional) Whether you can select multiple media entities. Defaults to FALSE.
  * target_bundles: (Optional) If you want to restrict the which media types can be selected for the field. Defaults to
    all types.
  * view_mode: (Optional) Specifies what view mode the media entities will be rendered with. Defaults to "full".
  * summary_view_mode: (Optional) Specifies what view mode the media entities will be rendered with in the
    administration interface. Defaults to "media_library".

This field can allow the administrator to select a single or multiple media entities using an autocomplete field. Sadly
Drupal Core still does not support using Media Library outside of fields yet.

### icon (included in Media Icons)

* Tokens: no
* Value: Icon ID
* Options:
  * width: (Optional) The width of the icon when displayed. Defaults to 100.
  * height: (Optional) The height of the icon when displayed. Defaults to 100.

This field can allow the administrator to select a single icon using the icon library.

## Advanced usage

Should the need for more advanced usage of Atoms be required then it's recommended to use the AtomsViewBuilder service.
Using this service you get access to ViewableAtom objects via the get() method and these are much more adjustable than
simply using Atom::get().

As a shortcut you can use Atom::getView() to get a ViewableAtom without having to get the AtomsViewBuilder service.

ViewableAtom objects allow you to set the following properties before rendering the Atom:

* **SetOption(key, value):** You can set an option otherwise specified in the atoms.yml file. Maybe in certain
  situations you want to change things.
* **SetOptions(values):** Same as the above but using an associative array with all options for the Atom at once.
* **SetTokenData(data):** If more than just the globally available tokens must be accessible, the appropriate data must
  be delivered with this method.

It also gives you access to lots of information useful rendering or other usage:

* **getCacheableMetadata():** The CacheableMetadata needed to render this Atom including tokens and with cache tags for
  all depending content. Useful for when you use Atom as a string and need to add its cacheability yourself.
* **getLangcode():** Sometimes a translation is not available for a requested language. This method returns the langcode
  found to fit the best.

And lastly you have several options for rendering or other output:

* **toRenderable():** The standard rendering method as a render array. Same as atom().
* **toString():** The rendered atom but as a string. Same as atom_str().
* **toValue(key = ""):** Maybe you just want the value of the field and not any rendered version. The type of value
  returned depends on the field type as seen above.
